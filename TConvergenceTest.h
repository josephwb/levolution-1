/*
 * TConvergenceTest.h
 *
 *  Created on: Feb 17, 2012
 *      Author: wegmannd
 */

#ifndef TCONVERGENCETEST_H_
#define TCONVERGENCETEST_H_

#include "TSigmaN.h"

class TConvergenceTest{
public:
	double* values;
	int numValues;
	int nextVal;
	bool full;
	double meanVal;
	double Sxx;
	double Sxy;
	double sumSquared;
	double T, N;

	TConvergenceTest(){
		numValues = 0;
		values = NULL;
		nextVal = 0;
		full = false;
		meanVal=0;
		Sxx = 0.0;
		Sxy = 0.0;
		sumSquared = 0.0;
		T = -999999.0;
		N = -1.0;
	};

	~TConvergenceTest(){
		if(numValues>0) delete[] values;
	};

	TConvergenceTest(int NumValues){
		numValues=NumValues;
		values = new double[numValues];
		nextVal = 0;
		full = false;
		meanVal = 0.0;
		Sxx = numValues * (numValues * numValues - 1.0) / 12.0;
		Sxy = 0.0;
		sumSquared = 0.0;
		T = -999999.0;
		N = -1.0;
	};

	void initialize(int NumValues){
		numValues=NumValues;
		values = new double[numValues];
		Sxx = numValues * (numValues * numValues - 1.0) / 12.0;
	};

	void clear(){
		nextVal = 0;
		full = false;
		meanVal = 0.0;
		Sxy = 0.0;
		sumSquared = 0.0;
		T = -999999.0;
		N = -1.0;
	};

	void resetToLast(int num){
		//check if there are enough values
		if(num < numValues){ //else keep all
			if(num < nextVal || full){ //else also keep all
				//remove num values -> recompute things for last num values
				//where to start
				int index = nextVal - num;
				if(index < 0) index = numValues + index;

				//copy values into new container
				double* tmp = new double[num];
				for(int j=0; j<num; ++j){
					tmp[j] = values[index];
					++index;
					if(index == numValues) index = 0;
				}

				//now add them again
				nextVal = 0;
				full = false;
				meanVal = 0.0;
				Sxy = 0.0;
				T = -999999.0;
				N = -1.0;
				for(int j=0; j<num; ++j){
					computeT(tmp[j]);
				}
			}

		}
	};

	void resetToHalf(){
		resetToLast(numValues / 2);
	};

	void computeT(const double & val){
		//adjust summaries
		if(full){
			meanVal -= values[nextVal]/(double) numValues;
			Sxy += 0.5 * ((double)numValues + 1.0 - 2.0*((double)nextVal+1.0))*values[nextVal];
			sumSquared -= values[nextVal] * values[nextVal];
		}
		meanVal+=val/(double)numValues;
		Sxy-=0.5 * ((double)numValues + 1.0 - 2.0*((double)nextVal+1))*val;
		sumSquared+=val * val;

		//store value
		 values[nextVal]=val;
		 ++nextVal;
		 if(nextVal==numValues){
			 nextVal=0;
			 full=true;
		 }

		 //check convergence -> return false if array is not full
		 if(full){
			 //perform test
			 double betaHat=Sxy/Sxx;
			 double sigmaSquared=1.0/((double)numValues-2.0) * (sumSquared - (double) numValues*meanVal*meanVal - betaHat * Sxy);
			 double se=sqrt(sigmaSquared / Sxx);
			 T=betaHat / se;
			 if(T<0.0) T=-T;
		 }
	}

	double getAbsT(){
		if(full) return T;
		else return -999999.0;
	};

	void computePablosN(){
		//calculate the fraction of switches in the slope
		//get sign of last slope
		if(full){
			int index = nextVal - 1;
			if(index < 0) index = numValues - 1;
			int previousIndex = nextVal - 2;
			if(previousIndex < 0) previousIndex = numValues - 1;
			bool sign, lastSign;
			if(values[index] > values[previousIndex]) lastSign = true;
			else lastSign = false;
			int numSignChanges = 0;

			//now go through all other slopes sequentially
			for(int i=0; i<(numValues-2); ++i){
				//get index
				index = previousIndex;
				previousIndex = index - 1;
				if(previousIndex < 0) previousIndex = numValues - 1;

				//calc slope
				if(values[index] > values[previousIndex]) sign = true;
				else sign = false;
				if(sign != lastSign) ++numSignChanges;
				lastSign = sign;
			}

			//return fraction
			N = (double) numSignChanges / (double) (numValues - 2);
		}
	};

	double getN(){
		if(full) return N;
		else return 0.0;
	};
};

class TConvergenceTestVector{
public:
	TConvergenceTest* tests;
	double maxT, minN;
	double trendCheckMaxT, trendCheckMinN;
	bool full;
	int numValues;


	TConvergenceTestVector(int NumValues, double MaxT, double MinN){
		numValues = NumValues;
		tests = new TConvergenceTest[3];
		for(int i=0; i<3; ++i) tests[i].initialize(numValues);
		maxT = 0.0;
		minN = 0.0;
		full = false;
		trendCheckMaxT = MaxT;
		trendCheckMinN = MinN;
	};

	void add(const double & val1, const double & val2, const double & val3){
		tests[0].computeT(val1);
		tests[1].computeT(val2);
		tests[2].computeT(val3);

		tests[0].computePablosN();
		tests[1].computePablosN();
		tests[2].computePablosN();

		if(tests[0].full){
			full = true;
			maxT = tests[0].getAbsT();
			if(tests[1].getAbsT() > maxT) maxT = tests[1].getAbsT();
			if(tests[2].getAbsT() > maxT) maxT = tests[2].getAbsT();

			minN = tests[0].getN();
			if(tests[1].getN() < minN) minN = tests[1].getN();
			if(tests[2].getN() < minN) minN = tests[2].getN();
		} else {
			maxT = -1.0;
			minN = 0.0;
		}
	};

	void add(TlevyParams* params){
		add(params->mu, params->s_0_squared, params->lambda);
	};

	void addToFile(std::ofstream & out){
		if(full){
			//T
			out << "\t" << tests[0].getAbsT();
			out << "\t" << tests[1].getAbsT();
			out << "\t" << tests[2].getAbsT();
			out << "\t" << maxT;

			//N
			out << "\t" << tests[0].getN();
			out << "\t" << tests[1].getN();
			out << "\t" << tests[2].getN();
			out << "\t" << minN;
		} else {
			out << "\t-\t-\t-\t-\t-\t-\t-\t-";
		}
	};

	void printToLog(TLog* logfile){
		if(full){
			logfile->flushNumberFixedWidth(maxT, 2, 7);
			logfile->flushNumberFixedWidth(minN, 2, 7);
		} else {
			logfile->flushFixedWidth('-', 7);
			logfile->flushFixedWidth('-', 7);
		}
	};

	bool checkTrend(){
		if(!full) return false;
		if(maxT < trendCheckMaxT && minN > trendCheckMinN) return true;
		return false;
	};

	void clear(){
		tests[0].clear();
		tests[1].clear();
		tests[2].clear();
		full = false;
	};

	void resetTo(int num){
		tests[0].resetToLast(num);
		tests[1].resetToLast(num);
		tests[2].resetToLast(num);
		full = false;
	};

	void resetToHalf(){
		tests[0].resetToHalf();
		tests[1].resetToHalf();
		tests[2].resetToHalf();
		full = false;
	};
};


#endif /* TCONVERGENCETEST_H_ */
