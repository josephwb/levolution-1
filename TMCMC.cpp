/*
 * TMCMC.cpp
 *
 *  Created on: Sep 16, 2014
 *      Author: wegmannd
 */

#include "TMCMC.h"

//-------------------------------------------------------------------
//TMCMCBase
//-------------------------------------------------------------------
TMCMCBase::TMCMCBase(TTree* Tree, TVariables* Vars, TRandomGenerator* RandomGenerator, TLog* Logfile){
	tree = Tree;
	randomGenerator = RandomGenerator;
	logfile = Logfile;
	vars = Vars;
	heat = 1.0; //default chain is not heated!

	//variables to be reset
	SigmaVectorInitialized = false;
	numSampled = 0;
	levyParams = NULL;
	curSigmaNvector = NULL;
	numAccepted = 0;
	acceptanceRate = 0.0;
	prog = 0;
	oldProg = 0;
	iteration = 0;
	length = 0;
	starttime = clock();
	verboseProgressConclusion = true;
}

void TMCMCBase::reset(TlevyParams* LevyParams){
	levyParams = LevyParams;
	vars->computeX(levyParams->mu);

	numSampled = 0;
	numAccepted = 0;
	prog = 0.0;
	oldProg = 0.0;

	if(!SigmaVectorInitialized) initCurSigmaNvector();
	curSigmaNvector->reset(levyParams);
}

void TMCMCBase::initCurSigmaNvector(){
	curSigmaNvector = new TSigmaN(tree, vars, randomGenerator);
	SigmaVectorInitialized = true;
}

void TMCMCBase::prepareMCMCStep(){
	curSigmaNvector->prepareMCMCStep();
}

void TMCMCBase::runMCMCStep(){
	numAccepted += curSigmaNvector->performSimpleMCMCStep(heat);
}

void TMCMCBase::startProgressReport(){
	starttime = clock();
	logfile->listFlush("Running MCMC ...");
}

void TMCMCBase::reportProgress(){
	prog = 100.0 * (float) iteration/ (float) length;
	if(prog > oldProg){
		logfile->listOverFlush("Running MCMC ... (" + toString(prog) + "%)");
		oldProg = prog;
	}
}
void TMCMCBase::concludeProgressReport(){
	if(verboseProgressConclusion){
		float runtime = (float) (clock()-starttime)/CLOCKS_PER_SEC / 60.0;
		logfile->overList("Running MCMC ... done (" + toString(runtime) + "min)!");
		concludeAcceptance();
	}
}
void TMCMCBase::concludeAcceptance(){
	acceptanceRate = (double) numAccepted / (double) (length * curSigmaNvector->numBranches);
	logfile->conclude("Acceptance rate was " + toString(floor(10000.0 * acceptanceRate)/1000.0) + "%");
}



//-------------------------------------------------------------------
//TMCMC
//-------------------------------------------------------------------
TMCMC::TMCMC(TTree* Tree, TVariables* Vars, TRandomGenerator* RandomGenerator, TLog* Logfile):TMCMCBase(Tree, Vars, RandomGenerator, Logfile){
	curSigmaNvectorEM = NULL;
}

void TMCMC::initCurSigmaNvector(){
	curSigmaNvectorEM = new TSigmaN_EM(tree, vars, randomGenerator);
	curSigmaNvector = curSigmaNvectorEM;
	SigmaVectorInitialized = true;
}

void TMCMC::runMCMC(long & Length, long & burnin, int & thinning){
	//other variables
	double newLambda = 0;
	length = Length;
	startProgressReport();

	for(iteration=0; iteration<length; ++iteration){
		//curSigmaNvector->performSimpleMCMCStep();
		prepareMCMCStep();
		runMCMCStep();

		//store thinned values
		if(iteration >= burnin && iteration % thinning==0){
			++numSampled;
			newLambda += curSigmaNvector->numJumpsOnTree;
			curSigmaNvectorEM->SMultiplyCoef = curSigmaNvectorEM->SMultiplyCoef + 1.0;
			reportProgress();
		}
	}
	acceptanceRate = (double) numAccepted / (double) (length*curSigmaNvectorEM->numBranches);

	//update parameters
	newLambda = newLambda/(double) numSampled / tree->getTotalTreeLength();
	if(newLambda / levyParams->lambda > 100.0) levyParams->lambda *=100.0;
	else levyParams->lambda = newLambda;

	curSigmaNvector->fastMultipleAddMatricesSim();
	curSigmaNvectorEM->S = curSigmaNvectorEM->S/(double) numSampled;
	levyParams->mu = (vars->ones_transposed * curSigmaNvectorEM->S * vars->x_orig).AsScalar()/(vars->ones_transposed*curSigmaNvectorEM->S*vars->ones).AsScalar();
	Matrix tempMat = (vars->x_orig - levyParams->mu*vars->ones);
	levyParams->s_0_squared = (tempMat.t()*curSigmaNvectorEM->S*tempMat).AsScalar() / (double) tree->getNumLeaves();
	concludeProgressReport();
}

void TMCMC::runMCMCForConvergenceTesting(long & Length, long & burnin, int & thinning, std::ofstream & outputFile){
	//other variables
	double newLambda = 0;
	length = Length;
	startProgressReport();
	Matrix tempMat;

	//write header
	outputFile << "Iteration\tRootState\tBrownVar\tLambda\n";

	for(iteration=0; iteration<length; ++iteration){
		//curSigmaNvector->performSimpleMCMCStep();
		prepareMCMCStep();
		runMCMCStep();

		//store thinned values
		if(iteration >= burnin && iteration % thinning==0){
			++numSampled;
			newLambda += curSigmaNvector->numJumpsOnTree;
			curSigmaNvectorEM->SMultiplyCoef = curSigmaNvectorEM->SMultiplyCoef + 1.0;
			reportProgress();

			if(iteration > 2000){
				//Estimate current parameters and print to file
				outputFile << iteration;

				//root state
				TSigmaN_EM testtest(curSigmaNvectorEM);
				testtest.fastMultipleAddMatricesSim();
				testtest.S = testtest.S/(double) numSampled;
				outputFile << "\t" <<  (vars->ones_transposed * testtest.S * vars->x_orig).AsScalar()/(vars->ones_transposed*testtest.S*vars->ones).AsScalar();

				//s0_squared
				tempMat = (vars->x_orig - levyParams->mu*vars->ones);
				outputFile << "\t" << (tempMat.t()*testtest.S*tempMat).AsScalar() / (double) tree->getNumLeaves();

				//lambda
				outputFile << "\t" << newLambda/(double) numSampled / tree->getTotalTreeLength() << "\n";
			}
		}
	}
	acceptanceRate = (double) numAccepted / (double) (length*curSigmaNvectorEM->numBranches);
	concludeProgressReport();
}

//-------------------------------------------------------------------
//TMCMCLL
//-------------------------------------------------------------------
TMCMCLL::TMCMCLL(TTree* Tree, TVariables* Vars, TRandomGenerator* RandomGenerator, TLog* Logfile):TMCMCBase(Tree, Vars, RandomGenerator, Logfile){}

void TMCMCLL::runMCMC(long & Length, long & burnin, int & thinning){
	double eLogPhi = 0.0;
	levyParams->FisherInfo = 0.0;
	double n_by_lambda, tmp;
	startProgressReport();
	length = Length;

	for(long iteration=0; iteration<length; ++iteration){
		curSigmaNvector->prepareMCMCStep();
		runMCMCStep();

		//estimate LL as average Log(Phi(N))
		if(iteration >= burnin && iteration % thinning==0){
			eLogPhi += curSigmaNvector->getLogConditionalDensity();
			n_by_lambda = curSigmaNvector->numJumpsOnTree / levyParams->lambda;
			tmp = n_by_lambda - tree->getTotalTreeLength();
			levyParams->FisherInfo += (n_by_lambda / levyParams->lambda)  - tmp*tmp;
			++numSampled;
			reportProgress();
		}
	}
	acceptanceRate = (double) numAccepted / (double) (length*curSigmaNvector->numBranches);

	//estimate LL and Fisher
	levyParams->logLikelihood = eLogPhi/(double) numSampled;
	levyParams->FisherInfo /= numSampled;
	concludeProgressReport();
}

//-------------------------------------------------------------------
//TMCMCEmpiricalBayes
//-------------------------------------------------------------------
TMCMCEmpiricalBayes::TMCMCEmpiricalBayes(TTree* Tree, TVariables* Vars, TRandomGenerator* RandomGenerator, TLog* Logfile):TMCMCBase(Tree, Vars, RandomGenerator, Logfile){}

void TMCMCEmpiricalBayes::runMCMC(long & Length, long & burnin, int & thinning, int & intermediatePosteriorThinning, std::string & outname){
	jumpDB.emptyDB(tree);
	length = Length;
	startProgressReport();

	for(iteration=0; iteration<length; ++iteration){
		curSigmaNvector->prepareMCMCStep();
		runMCMCStep();

		if(iteration >= burnin && iteration % thinning==0){
			curSigmaNvector->addJumpsToDB(jumpDB);
			reportProgress();
		}

		if(iteration >= burnin && iteration % intermediatePosteriorThinning==0) writeIntermediatePosteriorTrees(outname);
	}
	acceptanceRate = (double) numAccepted / (double) (length * curSigmaNvector->numBranches);
	concludeProgressReport();
}

void TMCMCEmpiricalBayes::writeJumpPosteriors(std::string basename, bool verbose){
	//has jumps per branch
	std::string filename = basename + ".hasJumpsPerBranch.post";
	if(verbose){
		logfile->list("Writing per-branch posterior probabilities to have at least one jump to '" + filename + "'.");
	}
	std::ofstream out(filename.c_str());
	if(!out) throw "Failed to open file '" + filename + "'!";
	out << jumpDB.getPosteriorTree_hasJumps() << std::endl;
	out.close();

	//number jumps per branch
	filename = basename + ".numJumpsPerBranch.post";
	if(verbose){
		logfile->list("Writing per-branch posterior mean number of jumps to '" + filename + "'.");
	}
	out.open(filename.c_str());
	if(!out) throw "Failed to open file '" + filename + "'!";
	out << jumpDB.getPosteriorTree_numJumps() << std::endl;
	out.close();

	//has jumps per leave
	filename = basename + ".hasJumpsPerLeave.post";
	if(verbose){
		logfile->list("Writing per-leave posterior probabilities to have at least one jump to '" + filename + "'.");
	}
	out.open(filename.c_str());
	if(!out) throw "Failed to open file '" + filename + "'!";
	out << jumpDB.getPosteriorLeaves_hasJumps() << std::endl;
	out.close();

	//num jumps per leave
	filename = basename + ".numJumpsPerLeave.post";
	if(verbose){
		logfile->list("Writing per-leave posterior mean number of jumps to '" + filename + "'.");
	}
	out.open(filename.c_str());
	if(!out) throw "Failed to open file '" + filename + "'!";
	out << jumpDB.getPosteriorLeaves_hasJumps() << std::endl;
	out.close();
}

void TMCMCEmpiricalBayes::writeIntermediatePosteriorTrees(std::string & outname){
	//write posterior tree
	std::string basename = outname+"_";

	int nZeros = floor(log10(length)) - floor(log10(iteration));
	for(int i=0; i<nZeros; ++i) basename += "0";
	basename +=toString(iteration);

	writeJumpPosteriors(basename, false);
}

//-------------------------------------------------------------------
//TMCMCEmpiricalBayesNew
//-------------------------------------------------------------------

TMCMCEmpiricalBayesLongJumps::TMCMCEmpiricalBayesLongJumps(TTree* Tree, TVariables* Vars, TRandomGenerator* RandomGenerator, TLog* Logfile, double ProbLongJump, double ProbGeometric):TMCMCEmpiricalBayes(Tree, Vars, RandomGenerator, Logfile){
	probLongJump = ProbLongJump;
	probGeometric = ProbGeometric;
	acceptanceRateLongJumps = 0.0;
}

void TMCMCEmpiricalBayesLongJumps::runMCMC(long & Length, long & burnin, int & thinning, int & intermediatePosteriorThinning, std::string & outname){
	jumpDB.emptyDB(tree);
	TSigmaN* sigmaLongJump = new TSigmaN(tree, vars, randomGenerator);
	TSigmaN* sigmaTmp;
	long numLong = 0;
	long numAcceptedLong = 0;
	length = Length;
	startProgressReport();

	for(iteration=0; iteration<length; ++iteration){
		if(randomGenerator->getRand() < probLongJump){
			//Do a long MCMC jump: put random jumps on an empty tree
			++numLong;
			sigmaLongJump->reset(levyParams);
			sigmaLongJump->addJumpsToRandomBranchesGeometric(probGeometric);

			//calculate hastings
			double h = sigmaLongJump->calculateHastingForGeometricTransition(curSigmaNvector, probGeometric);

			//accept or reject
			if(randomGenerator->getRand() < h){
				//accept
				sigmaTmp = curSigmaNvector;
				curSigmaNvector = sigmaLongJump;
				sigmaLongJump = sigmaTmp;
				++numAcceptedLong;
			}
		} else {
			curSigmaNvector->prepareMCMCStep();
			runMCMCStep();
		}

		//sampling
		if(iteration >= burnin && iteration % thinning==0){
			curSigmaNvector->addJumpsToDB(jumpDB);
			reportProgress();
		}

		if(iteration >= burnin && iteration % intermediatePosteriorThinning==0) writeIntermediatePosteriorTrees(outname);

	}
	acceptanceRate = (double) numAccepted / (double) (length * curSigmaNvector->numBranches);
	acceptanceRateLongJumps = (double) numAcceptedLong / (double) numLong;
	delete sigmaLongJump;
	concludeProgressReport();
}

void TMCMCEmpiricalBayesLongJumps::concludeAcceptance(){
	TMCMCBase::concludeAcceptance();
	logfile->conclude("acceptance rate of long jumps was " + toString(floor(1000*acceptanceRateLongJumps)/10) + "%");
}

//-------------------------------------------------------------------
//TMCMCHeated
//A heated version that does not recorded anything beyond its state
//-------------------------------------------------------------------
TMCMCEmpiricalBayesHeated::TMCMCEmpiricalBayesHeated(TTree* Tree, TVariables* Vars, TRandomGenerator* RandomGenerator, TLog* Logfile, int NumChains, double DeltaT):TMCMCEmpiricalBayes(Tree, Vars, RandomGenerator, Logfile){
	//initialize other chains
	numChains = NumChains;
	heatedChains = new TMCMCBase*[numChains];
	heatedChains[0] = this;
	logfile->list("Will use a delta T of " + toString(DeltaT));
	std::string heats=toString(1.0);
	for(int i=1; i<numChains; ++i){
		heatedChains[i] = new TMCMCBase(Tree, Vars, RandomGenerator, logfile);
		//set heat
		double heat = 1.0 / (1 + DeltaT*i);
		heatedChains[i]->setHeat(heat);
		heats += " " + toString(heat);
	}
	logfile->conclude("Resulting in these temperature: " + heats);

	acceptanceRateSwaps = 0.0;
	numSwaps = 0;
};

void TMCMCEmpiricalBayesHeated::reset(TlevyParams* LevyParams){
	TMCMCBase::reset(LevyParams);
	//also reset heated chains
	for(int i=1; i<numChains; ++i){
		heatedChains[i]->reset(LevyParams);
	}
};

void TMCMCEmpiricalBayesHeated::initCurSigmaNvector(){
	TMCMCEmpiricalBayes::initCurSigmaNvector();
	//also init heated chains
	for(int i=1; i<numChains; ++i){
		heatedChains[i]->initCurSigmaNvector();
	}
};

bool TMCMCEmpiricalBayesHeated::swapChains(TMCMCBase* first, TMCMCBase* second){
	//calculate hastings
	double h_log;
	if(first->curSigmaNvector->numJumpsOnTree == second->curSigmaNvector->numJumpsOnTree){
		//no need for prior
		//check if at same state
		if(first->curSigmaNvector->hasSameJumps(second->curSigmaNvector)) return true;
		h_log = (first->heat - second->heat) * (second->curSigmaNvector->getLogConditionalDensity() - first->curSigmaNvector->getLogConditionalDensity());
	} else h_log = (first->heat - second->heat) * (second->getCurrentLogLikelihood() - first->getCurrentLogLikelihood());

	//swap?
	if(randomGenerator->getRand() < exp(h_log)){
		//swap SigmaN
		TSigmaN* tmpSigmaN = first->curSigmaNvector;
		first->curSigmaNvector = second->curSigmaNvector;
		second->curSigmaNvector = tmpSigmaN;
		return true;
	} else return false;

}

void TMCMCEmpiricalBayesHeated::runMCMC(long & Length, long & burnin, int & thinning, int & intermediatePosteriorThinning, std::string & outname){
	jumpDB.emptyDB(tree);
	numSwaps = 0;
	length = Length;
	startProgressReport();

	for(iteration=0; iteration<length; ++iteration){

		//first store random numbers to allow for parallelizing!
		for(int i=0; i<numChains; ++i)
			heatedChains[i]->prepareMCMCStep();

		//then run MCMC step
		//#pragma omp parallel for
		for(int i=0; i<numChains; ++i)
			heatedChains[i]->runMCMCStep();

		//attempt to swap states between neighboring chains (those with adjacent temperature)
		for(int thisChain=0; thisChain < (numChains-1); ++thisChain){
			if(swapChains(heatedChains[thisChain], heatedChains[thisChain+1])) ++numSwaps;
		}

		if(iteration >= burnin && iteration % thinning==0){
			curSigmaNvector->addJumpsToDB(jumpDB);
			reportProgress();
		}

		if(iteration >= burnin && iteration % intermediatePosteriorThinning==0) writeIntermediatePosteriorTrees(outname);

	}
	concludeProgressReport();
}

void TMCMCEmpiricalBayesHeated::concludeAcceptance(){
	TMCMCBase::concludeAcceptance();
	acceptanceRateSwaps = (double) numSwaps / (double) (length*(numChains-1));
	logfile->conclude("Acceptance rate of swaps " + toString(floor(1000*acceptanceRateSwaps)/10) + "%");
}

//-------------------------------------------------------------------

