/*
 * levolution_core.cpp
 *
 *  Created on: Jun 1, 2011
 *      Author: wegmannd
 */

#include "TLevolution_core.h"

//-------------------------------------------------------------------------------------------------------------------------
//TLevolution_core
//-------------------------------------------------------------------------------------------------------------------------
TLevolution_core::TLevolution_core(TParameters* MyParameters, TLog* Logfile){
	myParameters=MyParameters;
	AIC_difference=0;
	logfile = Logfile;

	//initialize random generator
	logfile->listFlush("Initializing random generator ...");
	if(myParameters->parameterExists("fixedSeed")){
		randomGenerator = new TRandomGenerator(myParameters->getParameterLong("fixedSeed"), true);
	} else if(myParameters->parameterExists("addToSeed")){
		randomGenerator = new TRandomGenerator(myParameters->getParameterLong("addToSeed"), false);
	} else randomGenerator = new TRandomGenerator();
	logfile->flush(" done with seed ");
	logfile->flush(randomGenerator->usedSeed);
	logfile->write("!");

	//set output prefix and suffix
	outname = myParameters->getParameterStringWithDefault("outName", "levolution");

	//initialize other variables
	tree=NULL;
	treeInitialized=false;
	vars=NULL;
	varsInitialized=false;
	maxBrownVar=-1;
	maxLambda=-1;
	minPoisProb=-1;
	maxNumJumps=-1;
	numProposals=-1;
	iterations=-1;
	maxIterations=-1;
	params=NULL;
	brownianVarianceNullModelMLE=-1;
	rootStateNullModelMLE=-1;
	nullModelLikelihood=-1;
	LRT_statistics=-1;
	LRT_pValue=-1;
	AIC_difference=-1;
	doSims=false;
	bestEStepWeight=-1;
	treeLength=-1;
	numNVectors=-1; numNVectorsLL=-1; numNVectorsEB=-1;
	MCMClength=-1; MCMClenghtLL=-1; MCMClenghtEB=-1;
	thinning = -1; thinningLL = -1; thinningEB = -1;
	trendCheckVector = NULL;
	trendCheckInitialized = false;
	MCMCburninLength=-1;
	bestSigmaNvector=NULL;
}

void TLevolution_core::readNewickFromFile(std::string filename, std::string & newick){
	std::ifstream f;
	f.open(filename.c_str());
	if(!f) throw "File '" + filename + "' could not be opened!";

	std::string buf="";
	while(f.good() && !f.eof() &&  newick.find_first_of(";")==string::npos){
		getline(f, buf); // read the line
		newick+=buf;
	}
	f.close();
	buf.clear();

	//remove blanks and everything after ';'
	size_t a=newick.find_first_not_of(" \t");
	if(a!=string::npos) newick.erase(0, a);
	a=newick.find_first_of(";");
	if(a!=string::npos) newick.erase(a+1);
	a=newick.find_first_of(" \t");
	while( a!= string::npos){
		if(a!=string::npos) newick.erase(a,1);
		a=newick.find_first_of(" \t");
	}
}

void TLevolution_core::constructTreeFromFile(){
	//the file is assumed to contain a tree in Newick format, possibly spread over multiple lines
	//everything after the first ";" is ignored
	treeFile=myParameters->getParameterString("tree");
	logfile->listFlush("Reading phylogenetic tree from file '" + treeFile + " ...");
	std::string readNewick;
	readNewickFromFile(treeFile, readNewick);
	tree=new TTree(readNewick, randomGenerator);
	logfile->write(" done!");
	logfile->conclude("Read " + toString(tree->getNumNodes()) + " nodes and " + toString(tree->getNumLeaves()) + " leaves.");
	logfile->conclude("Total tree length = " + toString(tree->getTotalTreeLength()));
	if(tree->hasPolytomy()) throw "can not use tree: there are polytomies!";
	treeInitialized=true;
}

void TLevolution_core::simulateTree(){
	logfile->startIndent("Simulating a phylogeneic tree:");
	//simulating a tree with birthright, deathrate, age and numSpecies given
	logfile->startIndent("Using the following parameters:");
	double birthRate=myParameters->getParameterDouble("birthRate");
	logfile->list("birth rate = " + toString(birthRate));
	double deathRate=myParameters->getParameterDouble("deathRate");
	logfile->list("death rate = " + toString(deathRate));
	double age=myParameters->getParameterDouble("age");
	logfile->list("age = " + toString(age));
	int numLeaves = myParameters->getParameterDouble("numLeaves");
	logfile->list("numLeaves = " + toString(numLeaves));
	bool scale = false; double scaleTo = 1.0;
	if(myParameters->parameterExists("scaleTo")){
		scaleTo = myParameters->getParameterDouble("scaleTo");
		scale = true;
		logfile->list("Will scale tree to a total length of " + toString(scaleTo));
	}
	logfile->endIndent();

	//create tree
	logfile->listFlush("Simulating phylogenetic tree ...");
	tree = new TTree(numLeaves, birthRate, deathRate, age, randomGenerator);
	treeInitialized=true;
	tree->setRootDistanceToZero();
	if(scale) tree->scaleTree(scaleTo);
	logfile->write(" done!");
	logfile->conclude("Simulated a tree with total length " + toString(tree->getTotalTreeLength()));

	//write tree
	std::string filename=outname+".tree";
	logfile->listFlush("Writing tree to '" + filename + "' ...");
	std::ofstream f(filename.c_str());
	if(!f) throw "File 'tree.txt' could not be generated!";
	f << tree->getNewick() << endl;
	f.close();
	logfile->write(" done!");
	logfile->endIndent();
}

void TLevolution_core::performSimulations(){
	logfile->startIndent("Simulating Trait Values:");

	if(myParameters->parameterExists("tree")) constructTreeFromFile();
	else simulateTree();

	//read parameters
	logfile->startIndent("Using the following parameters:");
	double rootState=myParameters->getParameterDoubleWithDefault("rootState", 0.0);
	logfile->list("root state = " + toString(rootState));
	double brownVar=myParameters->getParameterDoubleWithDefault("brownVar", 1.0);
	logfile->list("brownian background rate = " + toString(brownVar));
	double alpha=myParameters->getParameterDouble("alpha");
	logfile->list("alpha = " + toString(alpha));

	//num jumps or lambda?
	if(myParameters->parameterExists("numJumps")){
		int numJumps=myParameters->getParameterInt("numJumps");
		logfile->list("number of jumps = " + toString(numJumps));
		logfile->endIndent();
		logfile->listFlush("Simulating trait values ...");
		tree->evoloveTraitLevyFixedNumJumps(rootState, brownVar, brownVar*alpha, numJumps);
	} else {
		double lambda;
		if(myParameters->parameterExists("lambdaScaled")){
			lambda=myParameters->getParameterDouble("lambdaScaled");
			logfile->list("lambda * tree length = " + toString(lambda));
			lambda = lambda / tree->getTotalTreeLength();
		} else {
			lambda=myParameters->getParameterDouble("lambda");
			logfile->list("lambda = " + toString(lambda));
		}
		logfile->endIndent();
		logfile->listFlush("Simulating trait values ...");
		tree->evoloveTraitLevy(rootState, brownVar, brownVar*alpha, lambda);
	}
	logfile->write(" done!");
	logfile->conclude("simulated " + toString(tree->getNumJumps()) + " poisson jumps");

	//write traits to file as well?
	logfile->startIndent("Writing output:");
	std::string filename = outname + ".traits";
	logfile->listFlush("Writing traits to '" + filename + "' ...");
	std::ofstream out(filename.c_str());
	tree->printLeaveNameTraitTable(out);
	out.close();
	logfile->write(" done!");

	//write poisson newick?
	if(myParameters->parameterExists("writeJumps")){
		//write location of jumps
		filename=outname+".jumps";
		logfile->listFlush("Writing jump locations to '" + filename + "' ...");
		out.open(filename.c_str());
		out << tree->getNewickJumps() << endl;
		out.close();
		logfile->write(" done!");

		//write jump strength
		filename=outname+".strength";
		logfile->listFlush("Writing jump strength to '" + filename + "' ...");
		out.open(filename.c_str());
		out << tree->getNewickValues() << endl;
		out.close();
		logfile->write(" done!");
	}

	//compute and write statistics
	if(myParameters->parameterExists("stats")){
		logfile->startIndent("Calculating summary statistics:");
		logfile->listFlush("Calculating statistics ...");
		vars=new TVariables(tree, logfile);
		varsInitialized=true;
		computeNullModelMLE();
		logfile->write(" done!");

		//write stats
		filename = outname + ".stats";
		logfile->listFlush("Writing statistics to '" + filename + "' ...");
		out.open(filename.c_str());
		out << "traitMean\ttraitVariance\trootStateNullModelMLE\tbrownianVarianceNullModelMLE\tnullModelLL" << std::endl;
		out << tree->getTraitMean() << "\t" << tree->getTraitVariance() << "\t" << rootStateNullModelMLE << "\t" << brownianVarianceNullModelMLE << "\t" << nullModelLikelihood << std::endl;
		out.close();
		logfile->write(" done!");
	}
	logfile->endIndent();
}

void TLevolution_core::computeNullModelMLE(){
	logfile->startIndent("Computing MLE estimates for null model:");
	//compute x with rootState 0;
	double rs_temp=0.0;
	vars->computeX(rs_temp);

	//precompute things
	int numLeaves = tree->getNumLeaves();
	double* tau_inv_store = vars->Tau_inv.Store();
	double* res = new double[numLeaves];
	double* res_x = new double[numLeaves];
	int start, end, resCol;
	for(int row = 0; row < numLeaves; ++row){
		end = (row*(row+3)) >> 1;
		res[row] = tau_inv_store[end];
		res_x[row] = res[row] * vars->x_array[row];
	}
	end = 0;
	for(int row = 1; row < numLeaves; ++row){
		start = end + 1;
		end = (row*(row+3)) >> 1;
		for(resCol=0; start<end; ++start, ++resCol){
			res[resCol] += tau_inv_store[start];
			res[row] += tau_inv_store[start];
			res_x[resCol] += vars->x_array[row] * tau_inv_store[start];
			res_x[row] += vars->x_array[resCol] * tau_inv_store[start];
		}
	}
	double ones_transposed_Tau_inv_x = 0.0;
	double ones_transposed_Tau_inv_ones = 0.0;
	double x_transposed_Tau_inv_x = 0.0;
	for(int row = 0; row < numLeaves; ++row){
		ones_transposed_Tau_inv_ones += res[row];
		ones_transposed_Tau_inv_x += res[row] * vars->x_array[row];
		x_transposed_Tau_inv_x += res_x[row] * vars->x_array[row];
	}
	delete[] res;
	delete[] res_x;

	//compute MLE
	rootStateNullModelMLE = ones_transposed_Tau_inv_x / ones_transposed_Tau_inv_ones;
	brownianVarianceNullModelMLE = 1.0/(double) (numLeaves-1.0) * (x_transposed_Tau_inv_x - ones_transposed_Tau_inv_x * ones_transposed_Tau_inv_x / ones_transposed_Tau_inv_ones);

	//compute Likelihood
	int length = numLeaves*(numLeaves+1) >> 1;
	double* S_inv_new = new double[length];
	for(int i=0; i<length; ++i) S_inv_new[i] = tau_inv_store[i] / brownianVarianceNullModelMLE;
	double S_det = numLeaves*log(brownianVarianceNullModelMLE) + vars->Tau_det_log;

	//compute x.t() * S_inv * x
	vars->computeX(rootStateNullModelMLE);
	nullModelLikelihood = vars->MulSym_xtMx(vars->x_array, S_inv_new, numLeaves);
	nullModelLikelihood = - 0.5 * (vars->pow2PiL_log + S_det) - 0.50 * nullModelLikelihood;
	delete[] S_inv_new;

	//report results
	logfile->list("MLE of root state = " + toString(rootStateNullModelMLE));
	logfile->list("MLE of Brownian variance = " + toString(brownianVarianceNullModelMLE));
	logfile->list("log-likelihood of null model = " + toString(nullModelLikelihood));
	logfile->endIndent();
}

void TLevolution_core::estimate(){
	logfile->startIndent("Estimating Lévy parameters:");

	//prepare estimation
	constructTreeFromFile();
	treeLength=tree->getTotalTreeLength();
	prepareEM();
	TlevyParams* bestLevyParams = new TlevyParams(1.0);
	TlevyParams* tmpLevyParams;

	//estimate null model
	computeNullModelMLE();

	//where to start?
	logfile->startIndent("Choosing initial starting values for EM:");
	if(myParameters->parameterExists("rootState")){
		bestLevyParams->mu = myParameters->getParameterDouble("rootState");
		logfile->list("Setting root state to " + toString(bestLevyParams->mu));
	} else {
		bestLevyParams->mu = rootStateNullModelMLE;
		logfile->list("Setting root state to null model MLE.");
		logfile->conclude("Root state = " + toString(bestLevyParams->mu));
	}
	if(myParameters->parameterExists("brownVar")){
		bestLevyParams->s_0_squared = myParameters->getParameterDouble("brownVar");
		logfile->list("Setting Brownian variance to " + toString(bestLevyParams->s_0_squared));
	} else {
		bestLevyParams->s_0_squared = brownianVarianceNullModelMLE;
		logfile->list("Setting Brownian variance to null model MLE.");
		logfile->conclude("Brownian variance = " + toString(bestLevyParams->s_0_squared));
	}
	if(myParameters->parameterExists("lambda")){
		bestLevyParams->lambda = myParameters->getParameterDouble("lambda");
		logfile->list("Setting jump rate lambda to " + toString(bestLevyParams->lambda));
	} else {
		bestLevyParams->lambda = 10.0 / treeLength;
		logfile->list("Setting jump rate lambda such that 10 events are expected on the tree.");
		logfile->conclude("lambda = 10 / " + toString(treeLength) + " = " + toString(bestLevyParams->lambda));
	}
	double initialLambda = bestLevyParams->lambda;
	bestLevyParams->logLikelihood = nullModelLikelihood;
	logfile->endIndent();

	//grid search?
	if(myParameters->parameterExists("alpha")){
		//read alphas to work with
		std::vector<double> alphaValues;
		readAlphaString(alphaValues);

		vector<double>::iterator alphaIt=alphaValues.begin();
		trendCheckVector->clear();

		//grid search if more than one alpha value is given
		for(;alphaIt!=alphaValues.end(); ++alphaIt){
			tmpLevyParams = new TlevyParams(bestLevyParams);
			tmpLevyParams->alpha = *alphaIt;
			if(bestLevyParams->lambda <= 0.1 / treeLength) tmpLevyParams->lambda = initialLambda;
			trendCheckVector->clear();

			runEM(tmpLevyParams);
			if(tmpLevyParams->logLikelihood > bestLevyParams->logLikelihood){
				delete bestLevyParams;
				bestLevyParams=tmpLevyParams;
			} else {
				delete tmpLevyParams;
			}
		}
	} else {
		//search using peak finder algo
		logfile->startIndent("Will use peak finder algorithm to optimize alpha:");
		double log10AlphaStart = myParameters->getParameterDoubleWithDefault("logAlphaStart", 1.0);
		double log10Alpha = log10AlphaStart;
		bestLevyParams->alpha = pow(10.0, log10Alpha);
		logfile->list("Will start at alpha = 10^" + toString(log10Alpha) + " = " + toString(bestLevyParams->alpha));
		double step = myParameters->getParameterDoubleWithDefault("logAlphaStep", 1.0);
		logfile->list("Will use initial step size of " + toString(step));
		int numOptimizations = myParameters->getParameterDoubleWithDefault("numOptimizations", 5);
		logfile->list("Will perform " + toString(numOptimizations) + " run of optimizations.");
		if (numOptimizations < 1.0) throw "At least one round of optimization has to be done.";
		double maxSearchRange = myParameters->getParameterDoubleWithDefault("logSearchRange",3.0);
		logfile->list("Will abort search if peak is not found within " + toString(maxSearchRange) + " orders of magnitude.");
		if (maxSearchRange < 1.0) throw "Search range has to be at least 1 order of magnitude";
		int counter = 0;
		logfile->endIndent();

		//run first
		runEM(bestLevyParams);

		//other params
		tmpLevyParams = new TlevyParams(bestLevyParams);
		TlevyParams* tmpLevyParams2 = new TlevyParams(bestLevyParams);
		TlevyParams* tmpSwap;
		bool reachedUpperLimit = false;
		bool reachedLowerLimit = false;

		//now run peak finder
		bool continueLoop = true;
		while(continueLoop){
			//propose new alpha and get LL
			tmpLevyParams2->initialize(tmpLevyParams);
			log10Alpha += step;
			tmpLevyParams2->alpha = pow(10.0, log10Alpha);

			//reset lambda in case it was zero before
			if(tmpLevyParams2->lambda == 0.0){
				if(bestLevyParams->lambda == 0.0) tmpLevyParams2->lambda = 0.01;
				else tmpLevyParams2->lambda = bestLevyParams->lambda;
			}

			//run EM
			//trendCheckVector->clear();
			trendCheckVector->resetToHalf();
			runEM(tmpLevyParams2);

			//do we continue?
			if(tmpLevyParams2->logLikelihood < tmpLevyParams->logLikelihood){
				++counter;
				if(counter == numOptimizations){
					continueLoop = false;
					break;
				} else {
					step = - step / 2.718282;
				}
			} else {
				//prevent under / overflow in case alpha = 0 or alpha=inf
				if(fabs(log10Alpha - log10AlphaStart) > maxSearchRange){
					if((log10Alpha > log10AlphaStart && reachedUpperLimit) || (log10Alpha < log10AlphaStart && reachedLowerLimit)){
						logfile->warning("Alpha search now more than 3 orders of magnitude away from start. Aborting search!");
						continueLoop = false;
						break;
					} else {
						step = - step;
						log10Alpha = log10AlphaStart;
						if(log10Alpha > log10AlphaStart) reachedUpperLimit = true;
						else reachedLowerLimit = true;
						logfile->warning("Alpha search now more than " + toString(maxSearchRange) + " orders of magnitude away from start. Searching in other direction!");
					}
				}
			}

			//swap
			tmpSwap = tmpLevyParams2;
			tmpLevyParams2 = tmpLevyParams;
			tmpLevyParams = tmpSwap;

			//keep best if best
			if(bestLevyParams->logLikelihood < tmpLevyParams->logLikelihood){
				bestLevyParams->initialize(tmpLevyParams);
			}
		}

		delete tmpLevyParams;
	}

	//write one line summary
	std::string filename=outname + "_oneLineSummary.txt";
	ofstream onelineSummary(filename.c_str());

	//write header of one line summary
	onelineSummary << "tree\ttraits\t";
	onelineSummary << "LL\troot_state\tbrown_variance\talpha\tlambda";
	onelineSummary << "\tnull_LL\tnull_root_state\tnull_brown_rate";
	onelineSummary << "\ttreeLength";//added by Pablo
	onelineSummary << "\tLRT_statistics\tLRT_pValue\tpreferredModelLRT\tAIC_difference\tpreferredModelAIC" << std::endl;
	//write used file names
	onelineSummary << treeFile << "\t" << traitFile << "\t";

	if(bestLevyParams->lambda > 0.0){
		logfile->startIndent("Parameters with largest log-likelihood:");
		logfile->list("root_state = " + toString(bestLevyParams->mu));
		logfile->list("brown_var = " + toString(bestLevyParams->s_0_squared));
		logfile->list("alpha = " + toString(bestLevyParams->alpha));
		logfile->list("lambda = " + toString(bestLevyParams->lambda));
		logfile->endIndent();

		//perform model choice
		performModelchoice(bestLevyParams);

		//write results of one line summary
		onelineSummary << bestLevyParams->logLikelihood;
		onelineSummary << "\t" << bestLevyParams->mu << "\t" << bestLevyParams->s_0_squared << "\t" << bestLevyParams->alpha << "\t" << bestLevyParams->lambda;
		onelineSummary << "\t" << nullModelLikelihood << "\t" << rootStateNullModelMLE << "\t" << brownianVarianceNullModelMLE;
		onelineSummary << "\t" << treeLength;//added by Pablo
		onelineSummary << "\t" << LRT_statistics << "\t" << LRT_pValue;
		if(LRT_pValue>0.05) onelineSummary << "\tnull";
		else onelineSummary << "\tlévy";
		onelineSummary << "\t" << AIC_difference;
		if(AIC_difference<0) onelineSummary << "\tnull";
		else onelineSummary << "\tlévy";
		onelineSummary << std::endl;

		if(myParameters->parameterExists("inferJumps"))
			estimateLocationOfJumps(bestLevyParams);

	} else {
		logfile->list("The EM algorithm converged at the null model.");
		logfile->conclude("Null model is preferred");

		//write results of one line summary
		onelineSummary << "NA\t" << bestLevyParams->mu << "\t" << bestLevyParams->s_0_squared << "\t" << bestLevyParams->alpha << "\t" << bestLevyParams->lambda;
		onelineSummary << "\t" << nullModelLikelihood << "\t" << rootStateNullModelMLE << "\t" << brownianVarianceNullModelMLE;
		onelineSummary << "\t" << treeLength;//added by Pablo
		onelineSummary << "\tNA\tNA\tNA\tnull\tnull" << std::endl;
	}
	onelineSummary.close();

	if(myParameters->parameterExists("llSurface"))
		computeLLSurface(bestLevyParams);

	delete bestLevyParams;
	logfile->endIndent();
}

void TLevolution_core::readAlphaString(vector<double> & alphaValues){
	logfile->startIndent("Parsing alpha values:");
	std::string alphastring=myParameters->getParameterString("alpha");
	eraseAllWhiteSpaces(alphastring);

	//alpha is either a single number, a list of integers separated by commas or a range with number such as 1-10:5
	if(stringContainsOnly(alphastring, "1234567890.")){
		//is a single number
		double alpha=stringToDouble(alphastring);
		if(alpha<=0.0) throw "Alpha has to be > 0!";
		alphaValues.push_back(alpha);
	} else {
		//multiple values are given
		vector<std::string> s;

		//maybe a list of values separated by commas?
		if(stringContainsOnly(alphastring, "1234567890.,")){
			fillVectorFromString(alphastring, alphaValues, ',');
			//check values
			for(vector<double>::iterator alphaIt=alphaValues.begin();alphaIt!=alphaValues.end(); ++alphaIt){
				if(*alphaIt <= 0.0) throw "Alpha has to be > 0!";
			}
		}
		//maybe a range?
		else if(alphastring.find('-')!=std::string::npos){
			logfile->list("Interpreting '" + alphastring + "' as a range");
			logfile->conclude("Performing grid search over alpha.");
			//is range
			if(alphastring.find(':')==std::string::npos) throw "Number of alpha values to be used is missing in alpha string '"+alphastring+"'. Use 'from-to:num' format.";
			fillVectorFromString(alphastring, s, ':');
			if(s.size()!=2) throw "Number of alpha values to be used is missing in alpha string '"+alphastring+"'. Use 'from-to:num' format.";
			int numAlpha=stringToInt(s[1]);
			if(numAlpha<2) throw "Number of alpha values in alpha string '"+alphastring+"' has to be at least 2 when giving a range! ";
			fillVectorFromString(s[0], s, '-');
			if(s.size()!=2) throw "Problem reading in alpha string '"+alphastring+"'. Use 'from-to:num' format.";
			double minAlpha=stringToDouble(s[0]);
			double maxAlpha=stringToDouble(s[1]);
			if(minAlpha>maxAlpha){
				double temp=maxAlpha;
				maxAlpha=minAlpha;
				minAlpha=temp;
			}
			if(minAlpha <= 0.0) throw "Alpha has to be > 0!";
			if(minAlpha == maxAlpha) throw "Alpha range has the same from and to values!";

			//prepare vector of alphas to be used
			bool logAlpha=myParameters->parameterExists("logAlpha");
			if(logAlpha){
				logfile->list("Distributing Alpha values uniformly on the log scale.");
				double alphaStep=(log10(maxAlpha)-log10(minAlpha))/(numAlpha-1);
				for(int i=0; i<numAlpha; ++i)
					alphaValues.push_back(pow(10, log10(minAlpha) + i*alphaStep));
			} else {
				double alphaStep=(maxAlpha-minAlpha)/(numAlpha-1);
				for(int i=0; i<numAlpha; ++i)
					alphaValues.push_back(minAlpha + i*alphaStep);
			}
		} else throw "Problem reading in alpha string '"+alphastring+"'. Give either a single value, a comma separated list of values (1.5,2,3,4.7) or a range (2-10:4).";
	}
	if(alphaValues.size()==1) logfile->list("Using alpha=" + toString(alphaValues[0]));
	else {
		std::string allAlpha;
		vector<double>::iterator alphaIt=alphaValues.begin();
		allAlpha=toString(*alphaIt); ++alphaIt;
		for(;alphaIt!=alphaValues.end(); ++alphaIt){
			allAlpha+=", " + toString(*alphaIt);
		}
		logfile->listFlush("Will test these alpha values: " + allAlpha);
		logfile->newLine();
	}
	logfile->endIndent();
}

void TLevolution_core::performModelchoice(TlevyParams* levyParams){
	logfile->startIndent("Computing model choice statistics:");
	//perform likelihood ratio test
	LRT_statistics = -2.0*(nullModelLikelihood - levyParams->logLikelihood);
	//compare to chi-sq table (too lazy to implement chi-sq, as it is anyway just and approximation)
	//interpolate between entries
	if(LRT_statistics<=0.0) LRT_pValue=1.0;
	else {
		if(LRT_statistics > 73.4736010){
			LRT_pValue = 1e-16;
		} else {
			double chisq[145]={0.0, 0.2107210, 0.4462871, 0.7133499, 1.0216512, 1.3862944, 1.8325815, 2.4079456, 3.2188758, 4.6051702, 4.8158912, 5.0514573, 5.3185201, 5.6268214, 5.9914645, 6.4377516, 7.0131158, 7.8240460, 9.2103404, 9.4210614, 9.6566275, 9.9236903, 10.2319916, 10.5966347, 11.0429218, 11.6182860, 12.4292162, 13.8155106, 14.0262316, 14.2617977, 14.5288604, 14.8371618, 15.2018049, 15.6480920, 16.2234562, 17.0343864, 18.4206807, 18.6314018, 18.8669678, 19.1340306, 19.4423320, 19.8069751, 20.2532622, 20.8286264, 21.6395566, 23.0258509, 23.2365720, 23.4721380, 23.7392008, 24.0475022, 24.4121453, 24.8584324, 25.4337965, 26.2447268, 27.6310211, 27.8417421, 28.0773082, 28.3443710, 28.6526724, 29.0173155, 29.4636026, 30.0389667, 30.8498969, 32.2361913, 32.4469123, 32.6824784, 32.9495412, 33.2578426, 33.6224857, 34.0687728, 34.6441369, 35.4550671, 36.8413615, 37.0520825, 37.2876486, 37.5547114, 37.8630127, 38.2276559, 38.6739430, 39.2493071, 40.0602373, 41.4465317, 41.6572528, 41.8928189, 42.1598817, 42.4681828, 42.8328259, 43.2791130, 43.8544771, 44.6654073, 46.0517017, 46.2624227, 46.4979888, 46.7650516, 47.0733529, 47.4379961, 47.8842832, 48.4596473, 49.2705775, 50.6568719, 50.8675880, 51.1031479, 51.3702345, 51.6785305, 52.0431662, 52.4894422, 53.0647879, 53.8757921, 55.2620865, 55.4728815, 55.7082626, 55.9754046, 56.2838117, 56.6481588, 57.0945569, 57.6701061, 58.4814064, 59.8665906, 60.0770650, 60.3123229, 60.5789894, 60.8904629, 61.2551060, 61.7013931, 62.2767572, 63.0876874, 64.4739818, 64.6847028, 64.9202689, 65.1873317, 65.4956330, 65.8602762, 66.3065633, 66.8819274, 67.6928576, 69.0791520, 69.3147181, 69.5817808, 69.8900822, 70.2547253, 70.2547253, 70.7010124, 71.2763766, 72.0873068, 73.4736011};
			double pvalue[145]={1.0, 9e-01, 8e-01, 7e-01, 6e-01, 5e-01, 4e-01, 3e-01, 2e-01, 1e-01, 9e-02, 8e-02, 7e-02, 6e-02, 5e-02, 4e-02, 3e-02, 2e-02, 1e-02, 9e-03, 8e-03, 7e-03, 6e-03, 5e-03, 4e-03, 3e-03, 2e-03, 1e-03, 9e-04, 8e-04, 7e-04, 6e-04, 5e-04, 4e-04, 3e-04, 2e-04, 1e-04, 9e-05, 8e-05, 7e-05, 6e-05, 5e-05, 4e-05, 3e-05, 2e-05, 1e-05, 9e-06, 8e-06, 7e-06, 6e-06, 5e-06, 4e-06, 3e-06, 2e-06, 1e-06, 9e-07, 8e-07, 7e-07, 6e-07, 5e-07, 4e-07, 3e-07, 2e-07, 1e-07, 9e-08, 8e-08, 7e-08, 6e-08, 5e-08, 4e-08, 3e-08, 2e-08, 1e-08, 9e-09, 8e-09, 7e-09, 6e-09, 5e-09, 4e-09, 3e-09, 2e-09, 1e-09, 9e-10, 8e-10, 7e-10, 6e-10, 5e-10, 4e-10, 3e-10, 2e-10, 1e-10, 9e-11, 8e-11, 7e-11, 6e-11, 5e-11, 4e-11, 3e-11, 2e-11, 1e-11, 9e-12, 8e-12, 7e-12, 6e-12, 5e-12, 4e-12, 3e-12, 2e-12, 1e-12, 9e-13, 8e-13, 7e-13, 6e-13, 5e-13, 4e-13, 3e-13, 2e-13, 1e-13, 9e-14, 8e-14, 7e-14, 6e-14, 5e-14, 4e-14, 3e-14, 2e-14, 1e-14, 9e-15, 8e-15, 7e-15, 6e-15, 5e-15, 4e-15, 3e-15, 2e-15, 1e-15, 9e-16, 8e-16, 7e-16, 6e-16, 5e-16, 4e-16, 3e-16, 2e-16, 1e-16};
			int x=0;
			while(chisq[x]<LRT_statistics){
				++x;
			}
			LRT_pValue=pvalue[x] + (chisq[x]-LRT_statistics)/(chisq[x]-chisq[x-1])*(pvalue[x-1]-pvalue[x]);
		}
	}
	logfile->list("Likelihood of null model = " + toString(nullModelLikelihood));
	logfile->list("Likelihood of best Lévy model = " + toString(levyParams->logLikelihood));
	logfile->list("Likelihood-Ratio-Test D = " + toString(LRT_statistics) + " (chi-square approx. p.value = " + toString(LRT_pValue) +" )");
	if(LRT_pValue>0.05) logfile->conclude("Null model is preferred");
	else logfile->conclude("Lévy model is preferred");


	//AIC
	AIC_difference=(4.0 - 2.0 * nullModelLikelihood) - (8.0 - 2.0 * levyParams->logLikelihood);
	logfile->list("Difference in Akaike information criterion = " + toString(AIC_difference) + " (null model - Lévy model)");
	if(AIC_difference<0) logfile->conclude("Null model is preferred");
	else logfile->conclude("Lévy model is preferred");
	logfile->endIndent();
}


void TLevolution_core::estimateLocationOfJumps(){
	logfile->startIndent("Estimating the location of jumps:");

	//read tree
	constructTreeFromFile();

	//read trait values and prepare variables
	traitFile=myParameters->getParameterString("traits");
	vars=new TVariables(tree, traitFile, logfile);
	varsInitialized=true;

	//read parameters
	logfile->startIndent("Using these levy parameters:");
	double rootState=myParameters->getParameterDouble("rootState");
	logfile->list("root state = " + toString(rootState));
	double brownVar=myParameters->getParameterDouble("brownVar");
	logfile->list("brownian background rate = " + toString(brownVar));
	double alpha=myParameters->getParameterDouble("alpha");
	logfile->list("alpha = " + toString(alpha));
	double lambda;
	if(myParameters->parameterExists("numJumps")){
		int numJumps = myParameters->getParameterDouble("numJumps");
		lambda = numJumps / tree->getTotalTreeLength();
		logfile->list("Will use lambda = " + toString(numJumps) + " jumps / total length of tree");
		logfile->conclude("lambda = " + toString(lambda));
	} else {
		lambda = myParameters->getParameterDouble("lambda");
		logfile->list("lambda = " + toString(lambda));
	}


	//create object
	TlevyParams levyParams(alpha);
	levyParams.lambda = lambda;
	levyParams.mu = rootState;
	levyParams.s_0_squared = brownVar;
	logfile->endIndent();

	//Estimating jumps
	estimateLocationOfJumps(&levyParams);
	logfile->endIndent();
}

void TLevolution_core::estimateLocationOfJumps(TlevyParams* levyParams){
	//estimate location of jumps using empirical Bayes
	logfile->startIndent("Estimating location of jumps:");

	//settings
	logfile->startIndent("MCMC settings:");
	std::string mcmcTypeEB = "standard";
	if(myParameters->parameterExists("mcmcTypeEB")) mcmcTypeEB = myParameters->getParameterString("mcmcTypeEB");
	if(mcmcTypeEB != "standard" && mcmcTypeEB != "longJumps" && mcmcTypeEB != "heated"){
		logfile->warning("Unknown MCMC type '" + mcmcTypeEB + "', using standard instead!");
		mcmcTypeEB = "standard";
	}

	//numNVecs
	if(myParameters->parameterExists("numNVecsEB")){
		numNVectorsEB = myParameters->getParameterDouble("numNVecsEB");
	} else if(myParameters->parameterExists("numNVecs")){
		numNVectorsEB = myParameters->getParameterDouble("numNVecs");
	} else {
		numNVectorsEB = 5000; //default values
	}
	logfile->list("Will sample " + toString(numNVectorsEB) + " vectors of jumps.");

	//thinning
	if(myParameters->parameterExists("thinningEB")){
		thinningEB = myParameters->getParameterDouble("thinningEB");
	} else if(myParameters->parameterExists("thinning")){
		thinningEB = myParameters->getParameterDouble("thinning");
	} else {
		thinningEB = 10; //default value
	}
	logfile->list("Will keep every " + toString(thinningEB) + "th step (thinning)");

	//burnin
	if(myParameters->parameterExists("burninEB")){
		MCMCburninLength = myParameters->getParameterDouble("burninEB");
		if(MCMCburninLength<0) throw "The requested burnin is < 1!";
	} else if(myParameters->parameterExists("burnin")){
		MCMCburninLength = myParameters->getParameterDouble("burnin");
		if(MCMCburninLength<0) throw "The requested burnin is < 1!";
	} else {
		MCMCburninLength = 1000;
	}
	logfile->list("Will discard the first " + toString(MCMCburninLength) + " steps as burnin");

	MCMClenghtEB=numNVectorsEB*thinningEB+MCMCburninLength;
	logfile->list("Will sample " + toString(numNVectorsEB) + " vectors of jumps to estimate the position of jumps.");
	logfile->conclude("The total length of this MCMC chain will thus be " + toString(MCMClenghtEB));
	logfile->endIndent();

	//create TMCMC object and run MCMC
	TMCMCEmpiricalBayes* mcmc;
	if(mcmcTypeEB == "standard"){
		logfile->startIndent("Running a standard MCMC:");
		mcmc = new TMCMCEmpiricalBayes(tree, vars, randomGenerator, logfile);
	} else if(mcmcTypeEB == "longJumps"){
		logfile->startIndent("Running an MCMC with long jumps:");
		double probLongJump = myParameters->getParameterDoubleWithDefault("probLongJumps", 0.1);
		logfile->list("Performing long jumps with probability " + toString(probLongJump));
		double probGeometric = myParameters->getParameterDoubleWithDefault("probGeometric", 0.95);
		logfile->list("Geometric probability " + toString(probGeometric));
		mcmc = new TMCMCEmpiricalBayesLongJumps(tree, vars, randomGenerator, logfile, probLongJump, probGeometric);
	} else if(mcmcTypeEB == "heated"){
		logfile->startIndent("Running a heated set of chains:");
		int numChains = myParameters->getParameterIntWithDefault("numChains", 3);
		logfile->list("Will use a total of " + toString(numChains) + " chains (" + toString(numChains-1) + " heated)");
		if(numChains<2) throw "The number of chains has to be at least two!";
		double deltaT = myParameters->getParameterDoubleWithDefault("deltaT", 0.2);
		mcmc = new TMCMCEmpiricalBayesHeated(tree, vars, randomGenerator, logfile, numChains, deltaT);
	} else throw "Unknown MCMC type '" + mcmcTypeEB + "'!";


	int intermediatePosteriorThinning =  myParameters->getParameterIntWithDefault("posteriorThinning", -1);
	if(intermediatePosteriorThinning < 0) intermediatePosteriorThinning = MCMClenghtEB + MCMCburninLength + 1; //i.e. do not print intermediate files

	//run MCMC
	mcmc->reset(levyParams);
	mcmc->runMCMC(MCMClenghtEB, MCMCburninLength, thinningEB, intermediatePosteriorThinning, outname);

	//writing posterior probabilities to file
	logfile->startIndent("Writing posterior probabilities:");
	mcmc->writeJumpPosteriors(outname, true);
	logfile->endIndent();
	logfile->endIndent();
	delete mcmc;
}

void TLevolution_core::computeLLSurface(TlevyParams* levyParams){
	//compute surface of LL for different lambda values using brute force (i.e. MCMC)
	logfile->startIndent("Calculating likelihood surface for lambda:");

	//open output
	std::string filename = outname + "_llSurface.txt";
	logfile->list("Will write surface to '" + filename + "'.");
	std::ofstream surface(filename.c_str());
	surface << "Lambda\tLL" << std::endl << levyParams->lambda << "\t" << levyParams->logLikelihood << std::endl;

	//how many lambdas?
	int numLambda = myParameters->getParameterDoubleWithDefault("surfacePos", 50.0) / 2.0;
	logfile->startIndent("Will calculate LL at " + toString(2*numLambda) + " positions.");
	vector<double> lambdaToTest;
	for(int i=1; i<(numLambda+1); ++i){
		lambdaToTest.push_back(levyParams->lambda*pow(1.127304, i));
		lambdaToTest.push_back(levyParams->lambda*pow(1.127304, -i));
	}
	sort(lambdaToTest.begin(), lambdaToTest.end());

	//run MCMC for each lambda
	TMCMCLL mcmc(tree, vars, randomGenerator, logfile);
	for(vector<double>::iterator it=lambdaToTest.begin(); it!=lambdaToTest.end(); ++it){
		logfile->startIndent("at lambda = " + toString(*it) + ":");
		levyParams->lambda=*it;
		mcmc.reset(levyParams);
		mcmc.runMCMC(MCMClenghtLL, MCMCburninLength, thinningLL);
		surface << *it << "\t" << levyParams->logLikelihood << std::endl;
		logfile->endIndent();
	}
	logfile->endIndent();
	logfile->endIndent();
}

void TLevolution_core::prepareEM(){
	//read trait values and prepare variables
	traitFile=myParameters->getParameterString("traits");
	vars=new TVariables(tree, traitFile, logfile);
	varsInitialized=true;

	//read parameters
	logfile->startIndent("Reading EM / MCMC settings:");
	if(myParameters->parameterExists("maxIterations")){
		maxIterations=myParameters->getParameterDouble("maxIterations");
		if(maxIterations<1) throw "The maximum number of iterations is set < 1!";
	}
	else maxIterations = 100;
	logfile->list("Will run EM for at maximum " + toString(maxIterations) + " iterations");

	//conditions to stop
	logfile->startIndent("The EM will be considered converged if:");

	double trendCheckMaxT = myParameters->getParameterDoubleWithDefault("maxT", 2);
	logfile->list("A trend could not be rejected with test statistics T < " + toString(trendCheckMaxT) + " for all parameters.");
	if(trendCheckMaxT<=0) throw "Threshold to reject trend T is <= 0!";

	double trendCheckMinN = myParameters->getParameterDoubleWithDefault("minN", 0.5);
	logfile->list("Random fluctuations were asserted with test statistics N > " + toString(trendCheckMinN) + " for all parameters.");
	if(trendCheckMinN < 0.0) throw "Threshold to assert random fluctuations N < 0!";
	if(trendCheckMinN > 1.0) throw "Threshold to assert random fluctuations N > 1!";

	int numValTrendCheck = myParameters->getParameterIntWithDefault("numValCheckTrend", 16);
	logfile->list("Trends and random fluctuations will be tested on the last " + toString(numValTrendCheck) + " values.");
	//if(numValTrendCheck<16) throw "Number of values to check for a trend in the EM is set < 16, which is too low for the approximation to the Student t-distribution.";
	if(numValTrendCheck<16) {//added by Pablo
		logfile->list("WARNING: Number of values to check for a trend in the EM is set < 16, which is too low for the approximation to the Student t-distribution.");
	}
	logfile->endIndent();

	trendCheckVector = new TConvergenceTestVector(numValTrendCheck, trendCheckMaxT, trendCheckMinN);
	trendCheckInitialized = true;

	//Parameters for EM MCMC
	logfile->startIndent("MCMC during EM algorithm:");
	numNVectors = myParameters->getParameterDoubleWithDefault("numNVecs", 5000);
	logfile->list("Will sample " + toString(numNVectors) + " vectors of jumps.");

	thinning=myParameters->getParameterDoubleWithDefault("thinning", 10);
	if(thinning<=0) throw "Thinning has to be larger than 0!";
	logfile->list("Will keep every " + toString(thinning) + "th step (thinning)");

	if(myParameters->parameterExists("burnin")){
		MCMCburninLength=myParameters->getParameterDouble("burnin");
		if(MCMCburninLength<0) throw "The requested burnin is < 1!";
	}
	else MCMCburninLength = 1000;
	logfile->list("Will discard the first " + toString(MCMCburninLength) + " steps as burnin");
	MCMClength=numNVectors*thinning+MCMCburninLength;
	logfile->list("The total length of each MCMC chain will thus be " + toString(MCMClength));
	logfile->endIndent();

	//Parameters for LL MCMC
	logfile->startIndent("MCMC to estimate log likelihood:");
	if(myParameters->parameterExists("numNVecsLL")) numNVectorsLL=myParameters->getParameterDouble("numNVecsLL");
	else numNVectorsLL = numNVectors;
	logfile->list("Will sample " + toString(numNVectorsLL) + " vectors of jumps.");

	thinningLL=myParameters->getParameterDoubleWithDefault("thinningLL", thinning);
	logfile->list("Will keep every " + toString(thinningLL) + "th step (thinning)");
	logfile->list("Will discard the first " + toString(MCMCburninLength) + " steps as burnin");

	MCMClenghtLL=numNVectorsLL*thinningLL+MCMCburninLength;
	if(numNVectorsLL != numNVectors) logfile->list("Will sample " + toString(numNVectorsLL) + " vectors of jumps to calculate the likelihood.");
	logfile->conclude("The total length of this MCMC chain will thus be " + toString(MCMClenghtLL));
	logfile->endIndent();

	logfile->endIndent();
}

void TLevolution_core::runEM(TlevyParams* levyParams){
	logfile->startIndent("Running EM algorithm for alpha=" + toString(levyParams->alpha) + ":");

	std::string filename = outname + "_EM_alpha_" + toString(levyParams->alpha) + ".txt";
	logfile->list("Will write EM also to '" + filename + "'.");
	std::ofstream EMfile(filename.c_str());
	EMfile << "Iteration\tRootState\tBrownVar\tLambda\tT_RootState\tT_BrownVar\tT_lambda\tmaxT\tN_RootState\tN_BrownVar\tN_lambda\tminN" << std::endl;

	//write header and starting values
	logfile->startNumbering("Running EM:");
	logfile->listFlush(" ", ' ');
	logfile->flushFixedWidth("root_state", 12);
	logfile->flushFixedWidth("brown_var", 12);
	logfile->flushFixedWidth("lambda", 12);
	logfile->flushFixedWidth("maxT", 7);
	logfile->flushFixedWidth("minN", 7);
	logfile->newLine();

	//run EM
	clock_t starttime;
	double runtime;
	TMCMC mcmc(tree, vars, randomGenerator, logfile);
	mcmc.verboseProgressConclusion = false;

	//output
	logfile->overFlush("");
	logfile->numberFlush("");
	//logfile->flushFixedWidth(i+1, 11);
	logfile->flushScientific(levyParams->mu, 3, 12);
	logfile->flushScientific(levyParams->s_0_squared, 3, 12);
	logfile->flushScientific(levyParams->lambda, 3, 12);
	logfile->flushFixedWidth('-', 7);
	logfile->flushFixedWidth('-', 7);
	runtime=(double) 0.0;
	logfile->writeFixedWidth("("+toString(round(runtime*10.0)/10.0)+"s)", 9);

	for(int i=0; i<maxIterations; ++i){
		starttime=clock();

		//run MCMC
		mcmc.reset(levyParams);
		mcmc.runMCMC(MCMClength, MCMCburninLength, thinning);

		//check if lambda is essentially zero: defined as < 0.1 jumps / tree
		if(levyParams->lambda < 0.05/treeLength) levyParams->lambda = 0.0;

		//compute trends
		trendCheckVector->add(levyParams);

		//output
		logfile->overFlush("");
		logfile->numberFlush("");
		//logfile->flushFixedWidth(i+1, 11);
		logfile->flushScientific(levyParams->mu, 3, 12);
		logfile->flushScientific(levyParams->s_0_squared, 3, 12);
		logfile->flushScientific(levyParams->lambda, 3, 12);
		trendCheckVector->printToLog(logfile);
		runtime=(double) (clock()-starttime)/CLOCKS_PER_SEC;
		logfile->writeFixedWidth("("+toString(round(runtime*10.0)/10.0)+"s)", 9);

		//output to file
		EMfile << i+1 << "\t" << levyParams->mu << "\t" << levyParams->s_0_squared << "\t" << levyParams->lambda;
		trendCheckVector->addToFile(EMfile);
		EMfile << std::endl;

		//stop iterations?
		if(levyParams->lambda==0.0 || trendCheckVector->checkTrend()) break;
	}
	logfile->endNumbering();
	EMfile.close();

	//add step to compute LLlogLikelihood
	if(levyParams->lambda > 0.0){
		logfile->startIndent("Estimating log-likelihood:");

		//run MCMC
		TMCMCLL mcmc(tree, vars, randomGenerator, logfile);
		mcmc.reset(levyParams);
		mcmc.runMCMC(MCMClenghtLL, MCMCburninLength, thinningLL);

		//report
		logfile->list("log-likelihood is " + toString(levyParams->logLikelihood));
		logfile->list("Observed Fisher information is " + toString(levyParams->FisherInfo));
		if(levyParams->FisherInfo > 0.0) logfile->conclude("Estimated std(lambda) is " + toString(1.0/sqrt(levyParams->FisherInfo)));
		else logfile->conclude("Fisher Information is <=0! Try running longer MCMC chains.");
		logfile->list("Poor mans std(lambda) is " + toString(sqrt(levyParams->lambda / tree->getTotalTreeLength())));
		logfile->endIndent();
	} else {
		logfile->list("MLE is at null Model -> rejecting Lévy.");
		levyParams->logLikelihood = -9999999999.0;
	}
	logfile->endIndent();
}

void TLevolution_core::writeOneLineSummary(){
	//print one line summary if details are not printed
	/*
	if(!verbose){
		//write header
		cout << "LL\troot_state\tbrown_variance\tpois_jump_var\tlambda";
		cout << "\tnull_LL\tnull_root_state\tnull_brown_rate";
		cout << "\tLRT_statistics\tLRT_pValue\tAIC_difference";
		cout << "\tmax_lambda\tmin_pois_prob\tmax_num_jumps";
		cout << "\tnum_configurations\tnum_proposals\tnum_iterations" << endl;
		//write results
		cout << logLikelihood;
		for(int p=0; p<4; ++p) cout << "\t" << params[p];
		cout << "\t" << nullModelLikelihood << "\t" << rootStateNullModelMLE << "\t" << brownianVarianceNullModelMLE;
		cout << "\t" << LRT_statistics << "\t" << LRT_pValue << "\t" << AIC_difference;
		if(maxNumJumps<0) cout << "\t" << maxLambda << "\t" << minPoisProb << "\tNA";
		else cout << "\tNA\tNA\t" << maxNumJumps;
		cout << "\t" << SigmaNvec.size() << "\t" << numProposals << "\t" << iterations << endl;
	}
	*/
}

void TLevolution_core::performFTest(){
	logfile->startIndent("Conducting F-test:");

	//read tree, trait values and prepare variables
	constructTreeFromFile();
	traitFile=myParameters->getParameterString("traits");
	vars=new TVariables(tree, traitFile, logfile);
	varsInitialized=true;

	//read tree of jumps
	std::string jumpTreeFile=myParameters->getParameterString("jumpTree");
	logfile->list("Reading tree with jumps from file '"  + treeFile + " ...");
	std::string readNewick;
	readNewickFromFile(jumpTreeFile, readNewick);
	TTree* jumpTree=new TTree(readNewick, randomGenerator);
	logfile->write(" done!");
	logfile->conclude("Read " + toString(tree->getNumNodes()) + " nodes and " + toString(tree->getNumLeaves()) + " leaves.");

	//read jump threshold
	double jumpThreshold=myParameters->getParameterDoubleWithDefault("jumpThreshold", 0.9);
	logfile->listFlush("Finding branches with jumps (threshold set to " + toString(jumpThreshold) + ") ...");

	//get list of branches with jumps
	vector<int> branchesWithJumps;
	for(int k=0; k < (int) jumpTree->getNumNodes(); ++k){
		if(jumpTree->getBranchLength(k) > jumpThreshold){
			branchesWithJumps.push_back(k);
		}
	}
	int K=branchesWithJumps.size();
	logfile->write(" done!");
	logfile->conclude("Found " + toString(branchesWithJumps.size()) + " branches with jumps.");

	int numLeaves =  tree->getNumLeaves();
	if(K>(numLeaves-2)) throw "More jumps than number of leaves - 1!";

	//construct X
	Matrix X(numLeaves, K+1);
	X=1.0;
	int col=2;
	for(vector<int>::iterator it=branchesWithJumps.begin(); it!=branchesWithJumps.end(); ++it, ++col){
		X.Column(col)=vars->uk[*it];
	}

	//estimate beta hat
	ColumnVector betaHat = (X.t() * vars->Tau_inv * X).i() * X.t() * vars->Tau_inv * vars->x_orig;

	//estimate s_square
	double* x_orig = vars->x_orig.Store();
	double* Tau_inv = vars->Tau_inv.Store();
	double s2 = vars->MulSym_xtMx(x_orig, Tau_inv,numLeaves);

	s2 = s2 - (vars->x_orig.t() * vars->Tau_inv * X * betaHat).AsScalar();
	s2 = s2 * 1.0/(numLeaves - K - 1.0);

	//compute F
	Matrix R(K, K+1);
	R = 0.0;
	for(int i=1; i<(K+1); ++i) R(i, i+1) = 1;

	double F = 1/(K*s2) * (betaHat.t() * R.t() * (R * (X.t() * vars->Tau_inv * X).i() * R.t()).i() * R * betaHat).AsScalar();

	//estimate model fit
	ColumnVector epsilon=vars->x_orig - X * betaHat;
	double xxx = (epsilon.t() * (s2 * vars->Tau).i()  * epsilon).AsScalar();

	//output
	logfile->list("F-Stat = " + toString(F));
	logfile->list("1 - pf(" +toString(F) + ", df1=" +toString(K) + ", df2=" +toString(tree->getNumLeaves()-K-1) + ")");
	logfile->list("Chi^2 = " + toString(xxx));
	logfile->list("1 - pchisq(" + toString(xxx) + ", df=" + toString(tree->getNumLeaves()-K-1) + ")");
	logfile->endIndent();
}


void TLevolution_core::testMCMCConvergence(){
	logfile->startIndent("Running MCMC for convergence testing:");

	std::string filename = outname + "_MCMC.txt";
	logfile->list("Will write MCMC to '" + filename + "'.");
	std::ofstream MCMCfile(filename.c_str());

	//read tree
	constructTreeFromFile();

	//read trait values and prepare variables
	traitFile = myParameters->getParameterString("traits");
	vars = new TVariables(tree, traitFile, logfile);
	varsInitialized = true;

	//read parameters
	logfile->startIndent("Using these lévy parameters:");
	double rootState=myParameters->getParameterDouble("rootState");
	logfile->list("root state = " + toString(rootState));
	double brownVar=myParameters->getParameterDouble("brownVar");
	logfile->list("Brownian background rate = " + toString(brownVar));
	double alpha=myParameters->getParameterDouble("alpha");
	logfile->list("alpha = " + toString(alpha));
	double lambda = myParameters->getParameterDouble("lambda");
	logfile->list("lambda = " + toString(lambda));
	logfile->endIndent();

	//create object
	TlevyParams levyParams(alpha);
	levyParams.lambda = lambda;
	levyParams.mu = rootState;
	levyParams.s_0_squared = brownVar;

	//Parse MCMC parameters
	logfile->startIndent("MCMC during EM algorithm:");
	numNVectors = myParameters->getParameterDoubleWithDefault("numNVecs", 1000);
	logfile->list("Will sample " + toString(numNVectors) + " vectors of jumps.");

	thinning=myParameters->getParameterDoubleWithDefault("thinning", 100);
	if(thinning<=0) throw "Thinning has to be larger than 0!";
	logfile->list("Will keep every " + toString(thinning) + "th step (thinning)");

	if(myParameters->parameterExists("burnin")){
		MCMCburninLength=myParameters->getParameterDouble("burnin");
		if(MCMCburninLength<0) throw "The requested burnin is < 1!";
	}
	else MCMCburninLength = 1000;
	logfile->list("Will discard the first " + toString(MCMCburninLength) + " steps as burnin");
	MCMClength=numNVectors*thinning+MCMCburninLength;
	logfile->list("The total length of each MCMC chain will thus be " + toString(MCMClength));
	logfile->endIndent();

	//Create and run MCMC
	TMCMC mcmc(tree, vars, randomGenerator, logfile);
	mcmc.verboseProgressConclusion = true;

	mcmc.reset(&levyParams);
	mcmc.runMCMCForConvergenceTesting(MCMClength, MCMCburninLength, thinning, MCMCfile);

	//close output
	MCMCfile.close();

	logfile->endIndent();
}
