/*
 * TMCMC.h
 *
 *  Created on: Sep 16, 2014
 *      Author: wegmannd
 */

#ifndef TMCMC_H_
#define TMCMC_H_

#include "TTree.h"
#include "TRandomGenerator.h"
#include "TSigmaN.h"
#include "TLog.h"

//-------------------------------------------------------------------
class TMCMCBase{
//-------------------------------------------------------------------
public:
	TLog* logfile;
	TRandomGenerator* randomGenerator;
	TSigmaN* curSigmaNvector;
	TlevyParams* levyParams;
	TTree* tree;
	TVariables* vars;
	bool SigmaVectorInitialized;
	double heat;
	double acceptanceRate;
	long iteration;
	long length;
	long numAccepted;
	bool verboseProgressConclusion;

	//temporary variables used during run
	long numSampled;
	int prog, oldProg;
	clock_t starttime;

	TMCMCBase(TTree* Tree, TVariables* Vars, TRandomGenerator* RandomGenerator, TLog* Logfile);
	virtual ~TMCMCBase(){
		if(SigmaVectorInitialized) delete curSigmaNvector;
	};
	virtual void reset(TlevyParams* LevyParams);
	virtual void initCurSigmaNvector();
	void setHeat(double Heat){heat = Heat;};
	double getCurrentLogLikelihood(){return curSigmaNvector->getLogLikelihood();};
	virtual void prepareMCMCStep();
	virtual void runMCMCStep();
	virtual void runMCMC(long & Length, long & burnin, int & thinning){throw "Base class TMCMCBase can not run MCMC!";};

	void startProgressReport();
	void reportProgress();
	void concludeProgressReport();
	virtual void concludeAcceptance();

};

//-------------------------------------------------------------------
class TMCMC:public TMCMCBase{
//-------------------------------------------------------------------
public:
	TSigmaN_EM* curSigmaNvectorEM;

	TMCMC(TTree* Tree, TVariables* Vars, TRandomGenerator* RandomGenerator, TLog* Logfile);
	void initCurSigmaNvector();
	void runMCMC(long & Length, long & burnin, int & thinning);
	void runMCMCForConvergenceTesting(long & Length, long & burnin, int & thinning, std::ofstream & outputFile);
};

//-------------------------------------------------------------------
class TMCMCLL:public TMCMCBase{
//-------------------------------------------------------------------
public:
	TMCMCLL(TTree* Tree, TVariables* Vars, TRandomGenerator* RandomGenerator, TLog* Logfile);
	virtual void runMCMC(long & Length, long & burnin, int & thinning);
};

//-------------------------------------------------------------------
class TMCMCEmpiricalBayes:public TMCMCBase{
//-------------------------------------------------------------------
public:
	TJumpDB jumpDB;

	TMCMCEmpiricalBayes(TTree* Tree, TVariables* Vars, TRandomGenerator* RandomGenerator, TLog* Logfile);
	virtual void runMCMC(long & Length, long & burnin, int & thinning, int & intermediatePosteriorThinning, std::string & outname);
	void writeJumpPosteriors(std::string basename, bool verbose);
	void writeIntermediatePosteriorTrees(std::string & outname);
};

//-------------------------------------------------------------------
class TMCMCEmpiricalBayesLongJumps:public TMCMCEmpiricalBayes{
//-------------------------------------------------------------------
public:
	double probLongJump;
	double probGeometric;
	double acceptanceRateLongJumps;

	TMCMCEmpiricalBayesLongJumps(TTree* Tree, TVariables* Vars, TRandomGenerator* RandomGenerator, TLog* Logfile, double ProbLongJump, double ProbGeometric);
	void runMCMC(long & Length, long & burnin, int & thinning, int & intermediatePosteriorThinning, std::string & outname);
	void concludeAcceptance();
};

//-------------------------------------------------------------------
class TMCMCEmpiricalBayesHeated:public TMCMCEmpiricalBayes{
//A heated version that does not recorded anything beyond its state
//-------------------------------------------------------------------
public:
	int numChains;
	TMCMCBase** heatedChains;
	double acceptanceRateSwaps;
	int numSwaps;

	TMCMCEmpiricalBayesHeated(TTree* Tree, TVariables* Vars, TRandomGenerator* RandomGenerator, TLog* Logfile, int NumChains, double DeltaT);
	~TMCMCEmpiricalBayesHeated(){
		for(int i=1; i<numChains; ++i) delete heatedChains[i];
		delete[] heatedChains;
	};
	void reset(TlevyParams* LevyParams);
	void initCurSigmaNvector();
	bool swapChains(TMCMCBase* first, TMCMCBase* second);
	void runMCMC(long & Length, long & burnin, int & thinning, int & intermediatePosteriorThinning, std::string & outname);
	void concludeAcceptance();
};


#endif /* TMCMC_H_ */
