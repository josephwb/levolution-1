/*
 * TSigmaN.h
 *
 *  Created on: Jul 2, 2011
 *      Author: wegmannd
 */

#ifndef TSIGMAN_H_
#define TSIGMAN_H_

#include "TTree.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <map>
//#include <algorithm>
#include "TRandomGenerator.h"
#include "TParameters.h"
#include "newmat.h"
#include "newmatap.h"
#include "newmatio.h"
#include <time.h>
#include <stdio.h>
#include <string.h>
#include "stringFunctions.h"

//--------------------------------------------------------------------------
class TlevyParams{
public:
	double mu;
	double s_0_squared;
	double lambda;
	double alpha;
	double logLikelihood;
	double FisherInfo;

	TlevyParams(double Alpha){
		alpha=Alpha;
		mu=0;
		s_0_squared=0;
		lambda=0;
		logLikelihood=0;
		FisherInfo=0;
	};
	TlevyParams(TlevyParams* other){
		initialize(other);
	};
	void operator=(TlevyParams & other){
		mu=other.mu;
		s_0_squared=other.s_0_squared;
		lambda=other.lambda;
		alpha=other.alpha;
		logLikelihood=other.logLikelihood;
		FisherInfo=other.FisherInfo;
	};
	void initialize(TlevyParams* other){
		mu=other->mu;
		s_0_squared=other->s_0_squared;
		lambda=other->lambda;
		alpha=other->alpha;
		logLikelihood=other->logLikelihood;
		FisherInfo=other->FisherInfo;
	};
};

//--------------------------------------------------------------------------
class TVariables{
public:
	SymmetricMatrix Tau;
	SymmetricMatrix Tau_inv;

	double Tau_det_log;
	double pow2PiL;
	double pow2PiL_log;
	ColumnVector* uk;
	Matrix* uk_transposed;

	//TODO: get rid of vector, use array!
	int numLeaves;
	ColumnVector x_orig;
	ColumnVector x;
	double* x_array;
	Matrix x_transposed;
	double oldRootState;
	Matrix ones;
	Matrix ones_transposed;

	//relations table
	int NrOfVectorRelations;
	double** PVectorsStore;
	int* PVectorsRel;
	int* PVectorsPop;
	int* PVectorsIdx;


	TVariables(TTree* tree, std::string filename, TLog* logfile);
	TVariables(TTree* tree, TLog* logfile);
	~TVariables(){
		delete[] uk;
		delete[] uk_transposed;
		delete[] x_array;
		delete[] PVectorsPop;
		delete[] PVectorsIdx;
		delete[] PVectorsRel;
		delete[] PVectorsStore;
	};
	void readTraitFile(std::string & filename, TTree* tree, TLog* logfile);
	void prepareVariables(TTree* tree, TLog* logfile);
	void computeX(const double & rootState, bool force=false);
	double MulSym_xtMx(double* x, double* M, int & lenX);
};

//--------------------------------------------------------------------------
//A DB to record the position of jumps during an MCMC
class TJumpDB{
public:
	TTree* tree;
	bool DBInitialized;
	long numRecords;

	//DB for branch specific posteriors
	int numBranches;
	int* jumpDBBranches_hasJump; //used to store how often each branch had at least one jump
	int* jumpDBBranches_numJumps; //used to store posterior mean on the number if jumps per branch

	//DB for leave specific posterior
	int numLeaves;
	std::vector<int>* branchToLeaveMap; //mapping branch indexes to the leaves for which this branch is parent.
	int* jumpDBLeaves_tmp;
	int* jumpDBLeaves_hasJump; //used to store how often the branches from the root to the tips have at least one jump
	int* jumpDBLeaves_numJumps; //used to store posterior mean on the number from the root to the tips


	TJumpDB();
	~TJumpDB(){
		if(DBInitialized){
			delete[] jumpDBBranches_hasJump;
			delete[] jumpDBBranches_numJumps;
			delete[] branchToLeaveMap;
			delete[] jumpDBLeaves_tmp;
			delete[] jumpDBLeaves_hasJump;
			delete[] jumpDBLeaves_numJumps;
		}
	};
	void emptyDB(TTree* Tree);
	void updateJumpDB(std::vector<int> & branchesWithJumps, int* jumpsPerBranch);
	std::string getPosteriorTree_hasJumps();
	std::string getPosteriorTree_numJumps();
	std::string getPosteriorLeaves_hasJumps();
	std::string getPosteriorLeaves_numJumps();
};

//--------------------------------------------------------------------------
class TSigmaN{
public:
	TRandomGenerator* randomGenerator;
	TTree* tree;
	int numBranches;
	int numLeaves;
	TVariables* vars;
	int* jumpsPerBranch;
	std::vector<int> branchesWithJumps;
	int numJumpsOnTree;
	double* traits_x;

	//alpha stuff
	double alpha, lambda, s_0_squared;
	SymmetricMatrix TNAlpha_inv;
	double* AlphaInvMatrixData;
	double* PVectorAlphaTemp;
	double TNAlpha_det_log;

	//variables for MCMC step (always overwritten during each step)
	//int chosenBranch_k, delta_n_k, n_k_plus;
	int Pop, LeafIdx, StartIdx;
	bool* delta_n_kBool;
	double* randomForHastings;
	int randomIndex;
	int numAccepted;

	TSigmaN(TTree* Tree, TVariables* Vars, TRandomGenerator* RandomGenerator);
	TSigmaN(TSigmaN* other);
	virtual ~TSigmaN(){
		delete[] jumpsPerBranch;
		delete[] PVectorAlphaTemp;
		delete[] randomForHastings;
		delete[] delta_n_kBool;
	};

	virtual void reset(TlevyParams* LevyParams);
	bool updateJumpsOnBranchDB(const int & branch, const int & jumps);
	void addJumpsToBranch(const int & branch, const int & jumps);
	void addJumpsToRandomBranchesGeometric(double & probGeometric);
	void setJumpsFrom(TSigmaN* other);
	bool hasSameJumps(TSigmaN* other);
	void writePoissonTree(std::string filename);
	int getTotalNumberOfJumps();
	void addJumpsToDB(TJumpDB & DB);
	std::string getJumpConfigForPrinting();
	void computeTNAlpha();
	void updateTNAlphaIteratively(const int & branch, const double delta_n_k);
	void fillPVectorAlphaTemp();
	double SparseMulSim_vtMv();
	double SparseMulSim_xtMv();
	virtual void SparseMulSimAdd_MvMvt(double Multiplier);
	virtual void fastMultipleAddMatricesSim(){throw "Function fastMultipleAddMatricesSim not defined in TSigmaN!";};
	//double getConditionalDensity(const double & brownianRate, const double & mu);
	double getLogConditionalDensity();
	double getLogPriorDensityOfJumps();
	double getLogLikelihood();
	double calculateHastingForGeometricTransition(TSigmaN* other, double & probGeometric);
	double calculatePriorRatioOneBranch(int & branch, TSigmaN* other);
	void prepareMCMCStep();
	int performSimpleMCMCStep(double heat = 1.0);
};

class TSigmaN_EM:public TSigmaN{
public:
	SymmetricMatrix S;
	double* PMatrixS;
	double SMultiplyCoef;

	TSigmaN_EM(TTree* Tree, TVariables* Vars, TRandomGenerator* RandomGenerator);
	TSigmaN_EM(TSigmaN_EM* other);
	~TSigmaN_EM(){};
	void reset(TlevyParams* LevyParams);
	void SparseMulSimAdd_MvMvt(double Multiplier);
	void fastMultipleAddMatricesSim();
};


#endif /* TSIGMAN_H_ */
