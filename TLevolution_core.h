/*
 * levolution_core.h
 *
 *  Created on: Jun 1, 2011
 *      Author: wegmannd
 */

#ifndef LEVOLUTION_CORE_H_
#define LEVOLUTION_CORE_H_

#include "TTree.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <map>
#include <algorithm>
#include "TRandomGenerator.h"
#include "TParameters.h"
#include "TSigmaN.h"
#include "newmat.h"
#include "newmatap.h"
#include "newmatio.h"
#include <time.h>
#include "TConvergenceTest.h"
#include <math.h>
#include "TMCMC.h"

class TLevolution_core{
private:
	TLog* logfile;
	TTree* tree;
	TParameters* myParameters;
	TVariables* vars;
	double maxBrownVar;
	double maxLambda;
	double minPoisProb;
	int maxNumJumps;
	int numNVectors, numNVectorsLL, numNVectorsEB;
	long MCMClength, MCMClenghtLL, MCMClenghtEB;
	int thinning, thinningLL, thinningEB;
	long MCMCburninLength;
	int numProposals;
	int iterations;
	int maxIterations;
	double* params;
	double brownianVarianceNullModelMLE;
	double rootStateNullModelMLE;
	double nullModelLikelihood;
	double LRT_statistics;
	double LRT_pValue;
	double AIC_difference;
	bool doSims;
	bool varsInitialized;
	bool treeInitialized;
	double bestEStepWeight;
	double treeLength;
	TConvergenceTestVector* trendCheckVector;
	bool trendCheckInitialized;

	std::string treeFile, traitFile;
	std::string outname;
	vector<TSigmaN*> SigmaNvec;
	TSigmaN* bestSigmaNvector;
	TRandomGenerator* randomGenerator;


public:
	TLevolution_core(TParameters* MyParameters, TLog* Logfile);
	virtual ~TLevolution_core(){
		if(treeInitialized) delete tree;
		if(varsInitialized) delete vars;
		delete randomGenerator;
		for(vector<TSigmaN*>::iterator i=SigmaNvec.begin(); i!=SigmaNvec.end(); ++i) delete (*i);
		if(trendCheckInitialized) delete trendCheckVector;
	};

	void computeNullModelMLE();
	void readNewickFromFile(std::string filename, std::string & newick);
	void constructTreeFromFile(std::string filename, TTree* storage);
	void constructTreeFromFile();
	void simulateTree();
	void performSimulations();
	void estimate();
	void readAlphaString(vector<double> & alphaValues);
	void performModelchoice(TlevyParams* levyParams);
	void estimateLocationOfJumps();
	void estimateLocationOfJumps(TlevyParams* levyParams);
	void computeLLSurface(TlevyParams* levyParams);
	void prepareEM();
	void runEM(TlevyParams* levyParams);
	void writeOneLineSummary();
	void performFTest();
	void testMCMCConvergence();
};

#endif /* LEVOLUTION_CORE_H_ */
