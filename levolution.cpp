/*
 * levolution.cpp
 *
 *  Created on: Jun 1, 2011
 *      Author: wegmannd
 */

//---------------------------------------------------------------------------
#include "TLevolution_core.h"
#include <time.h>
#include "TParameters.h"

//---------------------------------------------------------------------------
void showExplanations(){
	cout << "\n************************************************************************************"
		 << "\n* Levolution by Daniel Wegmann and Christoph Leuenberger                      	   *"
		 << "\n************************************************************************************"
	     << "\n* This program computes maximum likelihood estimates of trait evolution parameters *"
	     << "\n* from trait values of species assuming an underlying phylogenetic tree and the    *"
	     << "\n* trait to evolve under a Lévy process. See Wegmann et al. (2011) for more details.*"
	     << "\n*                                                                                  *"
	     << "\n* It takes the following arguments (default values are used for omitted arguments):*"
		 << "\n*    tree        : a file containing the rooted phylogenetic tree in Newick format *"
		 << "\n*    traits      : a file with a line for each leave of the tree where each line   *"
		 << "\n*                  contains two columns :                                          *"
		 << "\n*                    i : name of the leave (has to match a name in the tree)       *"
		 << "\n*                   ii : trait value                                               *"
		 << "\n*    maxLambda   : largest rate of the Poisson process to consider (default=0.1)   *"
		 << "\n*    minPoisProb : smallest probability of Poisson events considered (default=0.01)*"
		 << "\n*    maxNumJumps : largest number of jumps per branch (overriding minPoisProb)     *"
		 << "\n*    iterations  : maximum number of iterations to run (default=100)               *"
		 << "\n*    deltaLL     : min log-likelihood increase required to continue (default=10^-5)*"
		 << "\n*    numProposals: number of proposed parameter values per iterations (default=10) *"
		 << "\n*    verbose     : if given, the program prints all details instead of a summary   *"
		 << "\n*    addToSeed   : integer added to the seed of the random generator (default=0)   *"
		 << "\n*                                                                                  *"
		 << "\n* The program also allows to simulate trait values under a Lévy process on the     *"
		 << "\n* underlying phylogenetic tree. In addition to 'tree' and optionally 'verbose' and *"
		 << "\n* 'addToSeed', the following arguments are required:                               *"
		 << "\n*    rootState   : the trait value at the root of the tree (default=0)             *"
		 << "\n*    brownVar    : variance of the brownian process                                *"
		 << "\n*    poisJumpVar : variance of poisson jumps                                       *"
		 << "\n*    lambda      : rate of the poisson process                                     *"
		 << "\n*    oneLine     : if given, the results will be printed on one line (plus header) *"
		 << "\n*                                                                                  *"
		 << "\n* It is also possible to simulate trait values for a simulated tree. Trees can be  *"
		 << "\n* simulated using a birth and death rate and conditioning the tree on a given age  *"
		 << "\n* (distance from leaves to root) and the number of extant leaves. Instead of the   *"
		 << "\n* 'tree' argument use:                                                             *"
		 << "\n*    birthRate   : the rate at which new species arise                             *"
		 << "\n*    deathRate   : the rate at which species go extinct                            *"
		 << "\n*    age         : age (height) of the tree (distance from leaves to root). Note   *"
		 << "\n*                  that the root will older than the first branching event.        *"
		 << "\n*    numLeaves   : the number of extant leaves of the tree                         *"
		 << "\n*    writeTree   : tree (in Newick format) will be printed to the file 'tree.txt'  *"
		 << "\n************************************************************************************\n\n";
}

/*TODO:
- check const
- update passed values

*/
int main(int argc, char* argv[]){
	struct timeval start, end;
	gettimeofday(&start, NULL);

	TLog logfile;
	logfile.newLine();
	logfile.write(" Levolution ");
	logfile.write("************");

	try{
		if (argc<2) throw "No arguments provided!";

		//read parameters from the command line
		TParameters myParameters(argc, argv, &logfile);

		//verbose?
		bool verbose=myParameters.parameterExists("verbose");
		logfile.setVerbose(verbose);

		//open log file
		std::string  logFilename=myParameters.getParameterString("logFile", false);
		if(logFilename.length()>0){
			logfile.openFile(logFilename.c_str());
			logfile.writeFileOnly(" Levolution ");
			logfile.writeFileOnly("************");
		}

		if(!verbose) logfile.listNoFile("Running in silent mode (use 'verbose' to get a status report on screen)");


		//initialize core
		TLevolution_core levolution(&myParameters, &logfile);

		//what is to be done?
		std::string task=myParameters.getParameterString("task");
		if(task=="infer") levolution.estimate();
		else if(task=="simulate") levolution.performSimulations();
		else if(task=="F-test") levolution.performFTest();
		else if(task=="inferJumps") levolution.estimateLocationOfJumps();
		else if(task=="testMCMC") levolution.testMCMCConvergence();
		else throw "Unknown task '" + task +"'!";

		//write unsused parameters
		std::string unusedParams=myParameters.getListOfUnusedParameters();
		if(unusedParams!="") logfile.warning("The following parameters were not used: " + unusedParams + "!");
	 }
	catch (std::string & error){
		logfile.error(error);
		cerr << "run Levolution without arguments to see explanations." << endl << endl;
	}
	catch (const char* error){
		logfile.error(error);
		cerr << "run Levolution without arguments to see explanations." << endl << endl;
	}
	catch (...){
	    cerr << endl << endl << "ERROR: unhandeled error!" << endl;
	    cerr << "run Levolution without arguments to see explanations." << endl << endl;
    }
	logfile.clearIndent();
	gettimeofday(&end, NULL);
	float runtime=(end.tv_sec  - start.tv_sec)/60.0;
	logfile.list("Program terminated in ", runtime, " min!");
	logfile.close();
	return 0;
}
