/*
 * TTree.h
 *
 *  Created on: Feb 28, 2009
 *      Author: wegmannd
 */

#ifndef TTREE_H_
#define TTREE_H_

#include "stringFunctions.h"
#include <vector>
#include <map>
#include "TRandomGenerator.h"
#include <algorithm>
#include <sstream>
#include <iostream>


//---------------------------------------------------------------
class TBaseNode {
public:
	double distance; //is the distance to the parent...
	int jumps;
	double value;
	double trait;
	std::vector<TBaseNode*> children;
	std::string name;
	int numDescendantLeaves;
	int NodeIdx;
	int LeafIdx;
	int ParentNodeIdx;

	TBaseNode(){
		distance=0;
		jumps = 0;
		value=0.0;
		trait=0;
		numDescendantLeaves=0;
		NodeIdx=0;
		LeafIdx=0;
		ParentNodeIdx=0;
	};
	virtual ~TBaseNode(){};

	double getDistance(){ return distance; };
	void setDistance(double Distance){ distance=Distance; };
	void scale(double & scale){ distance *= scale; };
	void setValue(double & Val){value = Val;}
	void setJumps(int & Jumps){jumps = Jumps;}
	void sampleJumps(double & lambda, TRandomGenerator* randomGenerator);
	void simulateJumpEvolution(double & poissonVar, TRandomGenerator* randomGenerator);
	void simulateTraitEvolution(double & brownianVar, TRandomGenerator* randomGenerator);
	void addChild(TBaseNode* child){ children.push_back(child); };
	int getNumChildren(){return children.size();};
	double getValue(){return value;};
	//double getPoissonProb(const double & rate, const int & n);
	std::string getNewick();
	std::string getNewickValues();
	std::string getNewickJumps();
	void setTrait(double Trait){ trait=Trait;};
	double getTrait(){ return trait;};
	virtual TBaseNode* getParent(){return NULL;};
	virtual void propagateBrownianTrait(double & brownianVar, TRandomGenerator* randomGenerator){};
	virtual void propagateLevyTrait(double & brownianVar, double & poissonVar, double & lambda, TRandomGenerator* randomGenerator){};
	virtual void propagateLevyTraitFixedNumJumps(double & brownianVar, double & poissonVar, double & cumulLength, std::vector<double> & waitingTimes, std::vector<double>::iterator & waitingTimesIt, TRandomGenerator* randomGenerator){};
	virtual void saveAllParents(std::vector<TBaseNode*> p){};
	virtual bool isParent(TBaseNode* other){return false;};
	virtual double getDistanceToRoot(){ return 0.0;};
	virtual double getCumulativeValueToRoot(){ return 0;};
	virtual TBaseNode* getCommonParent(TBaseNode* other){return NULL;};
	virtual double getCommonBranchLength(TBaseNode* other){return -1;};
	int getNumDescendantLeaves(){return numDescendantLeaves;};
	int computeNumDescendantLeaves();
	virtual double getCumulativeValueCommonBranches(TBaseNode* other){return -1;};
	void setName(std::string Name){name=Name;};
	std::string getName(){return name;};
};
class TNode:public TBaseNode{
private:
	TBaseNode* parent;
	std::vector<TBaseNode*> allParents;
public:
	TNode(TBaseNode* Parent, double Distance, std::string Name=""){
		distance=Distance;
		parent=Parent;
		name=Name;
	};
	virtual void propagateBrownianTrait(double & brownianVar, TRandomGenerator* randomGenerator);
	virtual void propagateLevyTrait(double & brownianVar, double & poissonVar, double & lambda, TRandomGenerator* randomGenerator);
	virtual void propagateLevyTraitFixedNumJumps(double & brownianVar, double & poissonVar, double & cumulLength, std::vector<double> & waitingTimes, std::vector<double>::iterator & waitingTimesIt, TRandomGenerator* randomGenerator);
	TBaseNode* getParent(){return parent;};
	virtual void saveAllParents(std::vector<TBaseNode*> p);
	virtual bool isParent(TBaseNode* other);
	virtual TBaseNode* getCommonParent(TBaseNode* other);
	virtual double getDistanceToRoot(){return distance+parent->getDistanceToRoot();}
	virtual double getCumulativeValueToRoot(){return value+parent->getCumulativeValueToRoot();};
	virtual double getCommonBranchLength(TBaseNode* other);
	virtual double getCumulativeValueCommonBranches(TBaseNode* other);
};
class TRoot:public TBaseNode{
public:
	TRoot(){};
	TRoot(double Distance){distance=Distance;name="Root";}
	~TRoot(){};
	void saveAllParents();
	virtual TBaseNode* getParent(){return NULL;};
	virtual void propagateBrownianTrait(double & Trait, double & brownianVar, TRandomGenerator* randomGenerator);
	virtual void propagateLevyTrait(double & Trait, double & brownianVar, double & poissonVar, double & lambda, TRandomGenerator* randomGenerator);
	virtual void propagateLevyTraitFixedNumJumps(double & Trait, double & brownianVar, double & poissonVar, std::vector<double> & waitingTimes, std::vector<double>::iterator & waitingTimesIt, TRandomGenerator* randomGenerator);
	virtual bool isParent(TBaseNode* other){return false;};
	virtual double getDistanceToRoot(){return distance;}
	virtual double getCumulativeValueToRoot(){return value;}
	virtual TBaseNode* getCommonParent(TBaseNode* other){return this;};
	virtual double getCommonBranchLength(TBaseNode* other){return distance;};
	virtual double getCumulativeValueCommonBranches(TBaseNode* other){return 0;};
};
//---------------------------------------------------------------
//---------------------------------------------------------------
class TTree {
private:
	void makeChildNodes(std::string nexusSnipplet, TBaseNode* parent=NULL);
	std::string Newick;
	double probability;
	double traitMean, traitVar;
	double totLength;
	bool traitMeanComputed, traitVarComputed, totLengthcomputed, numDescendantLeavesComputed;
	void initialize(std::string & Nexus, double Probability, TRandomGenerator* RandomGenerator);

public:
	std::vector<TBaseNode*> nodes;
	std::map<std::string, TBaseNode*> leaves;
	TRoot* root;
	bool distancesKnown;
	bool allParentsSaved;

	TRandomGenerator* randomGenerator;

	TTree(){
		probability=1;
		traitMean=-1;
		traitVar=-1;
		totLength=-1;
		traitMeanComputed=false;
		traitVarComputed=false;
		totLengthcomputed=false;
		numDescendantLeavesComputed=false;
		distancesKnown=true;
		allParentsSaved=false;
		root=NULL;
		randomGenerator=NULL;
	};
	TTree(std::string Newick);
	TTree(std::string Newick, TRandomGenerator* RandomGenerator);
	TTree(std::string Newick, double Probability, TRandomGenerator* RandomGenerator);
	TTree(int & numSpecies, double birthrate, double deathrate, double age, TRandomGenerator* RandomGenerator);
	virtual ~TTree(){
		for(std::vector<TBaseNode*>::iterator i=nodes.begin(); i!=nodes.end(); ++i) delete (*i);
	};
	double getProbability(){ return probability; }
	unsigned int getNumLeaves();
	unsigned int getNumNodes();
	TBaseNode* getLeaveFromName(std::string name);
	int getLeaveNumberFromName(std::string name);
	TBaseNode* getParentOfNodeFromName(std::string name);
	double getBranchLengthFromName(std::string name);
	double getBranchLength(int node){return nodes[node]->distance;};
	std::string getNewick();
	std::string getNewickValues();
	std::string getNewickJumps();
	void evoloveTrait(double rootState, double brownianVariance);
	void evoloveTraitLevy(double rootState, double brownianVariance, double poissonVariance, double lambda);
	void evoloveTraitLevyFixedNumJumps(double rootState, double brownianVariance, double poissonVariance, int numJumps);
	double getTraitValue(int num);
	double getTraitMean();
	double getTraitVariance();
	double getCommonBranchLength(int i, int j);
	void sampleValuePoisson(double & lambda);
	void setValue(double Val);
	void setJumps(int jumps);
	void setRootDistanceToZero(){root->setDistance(0.0);};
	double getCumulativeValueCommonBranches(int i, int j);
	double getValueOneBranch(int num);
	double getCumulativeValue();
	int getNumJumps();
	void setValueOfBranch(int branch, double Val);
	void addValueToBranch(int branch, double Val);
	void setNumJumpsOnBranch(int branch, int jumps);
	bool isSubordinate(int node, int leave);
	int getNumDescendantLeaves(int node);
	double getTotalTreeLength();
	void scaleTree(double length);
	std::string getLeaveName(int leave);
	void printLeaveNamesToSTDout(std::string delim="\t");
	void printLeaveTraitValuesToSTDout(std::string delim="\t");
	void printLeaveNameTraitTable(std::string delim="\t");
	void printLeaveNameTraitTable(std::ofstream & out, std::string delim="\t");
	void printLeaveNameTraitTableWithPrefix(std::string prefix, std::string delim);
	int getRandomNodeWithBranchlengthWeights();
	int getRandomNode();
	bool hasPolytomy();
};

#endif /* TTREE_H_ */
